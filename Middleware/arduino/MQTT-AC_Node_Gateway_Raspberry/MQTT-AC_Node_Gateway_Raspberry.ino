#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include "esp_system.h"

const char* ssid = ""; //Gateway WiFi SSID Name
const char* password = ""; //Gateway WiFi Password
const char* server = ""; //Gateway WiFi Server IP

String deviceId = ""; //Registered Device ID (Device Node) in Authentication Server
String devicePass = ""; //Registered Device Password (Device Node) in Authentication Server
String topic = ""; //Registered MQTT Topic (Device Node to Device Gateway) in Authentication Server
String clientId = ""; //Client ID for MQTT connection between Device Node to Device Gateway

long randNumber;
String joinString;

//You can change with your own CA Certificate for HTTP Protocol
const char* ca_cert_http = \
"-----BEGIN CERTIFICATE-----\n" \
"MIIDTjCCAjagAwIBAgIJAJSa7eDdWLFUMA0GCSqGSIb3DQEBCwUAMDwxFDASBgNV\n" \
"BAMMCzE5Mi4xNjguNC4xMSQwIgYJKoZIhvcNAQkBFhVhdWxpYXdhcmRhbkBnbWFp\n" \
"bC5jb20wHhcNMTgwMjE4MTExNTU2WhcNMjgwMjE2MTExNTU2WjA8MRQwEgYDVQQD\n" \
"DAsxOTIuMTY4LjQuMTEkMCIGCSqGSIb3DQEJARYVYXVsaWF3YXJkYW5AZ21haWwu\n" \
"Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8LxiiyysS1dvWTs9\n" \
"K2AQ1fU0hyib5g0EqM+xwxgtfv6U1dI5ebF5yxyUStXIlMaWwpURuxYIrG5IQAw8\n" \
"15bIObiZmoxazfS4Je7OCDSA7tGUaUJRMV6RK7hev1S4GZ/8qcJjvdDxhbztGwpa\n" \
"gLnPtfAN0l+NLJm3tnPcFONUcOeMH7xp/jnxwsFUTEsutRmMhXhOC/Oc9ilMUhFl\n" \
"hA0Tz3wpFrDGlvrpm3w6+nk/6aYSeBSiEi9OpFCbOf/Nts7egc7CoTBCtmLeqEwn\n" \
"ScIQJzmkrewUFNWHBymRhvWTH0hyEbXmpn2M0FNFdBN9Bsoc/zDPQDlAVUA29VjQ\n" \
"DWFURwIDAQABo1MwUTAdBgNVHQ4EFgQU/RZVogaqJIoiGhSRPGpLmpbqfY4wHwYD\n" \
"VR0jBBgwFoAU/RZVogaqJIoiGhSRPGpLmpbqfY4wDwYDVR0TAQH/BAUwAwEB/zAN\n" \
"BgkqhkiG9w0BAQsFAAOCAQEAVFY5rZJPNY2fN0vbmVsxIX9zJBbd4HGjSDZqlbbQ\n" \
"Yt1wM5LSAKInvc+3m57tudUDLULFj8S17TNTEFzCB6Rpd5KUUwm3hjtrCQ6lbXub\n" \
"vZqab/tgXDwDv08cdcMzcbF5xcuKY0J3VgBZ+PQ+xi7b4KfNpNA2nIWO8IzFK8PC\n" \
"0kZDusl0wc0M4CNIJRjLKLoCFRE1EZR5QP/VrJR5rf4Z1Cep0iMmzMDOXk7LXi6W\n" \
"4lwvXyWeCr0b+Lt+oaINOCs213KYarhmp2A88XqRlJif5j/rSVx0Zwz4CLmT8Cxe\n" \
"tZ/YiktVGEXz/B3m3b5PsdpYP7/RSiTCrtoq3xn0O7VajA==\n" \
"-----END CERTIFICATE-----\n";

//You can change with your own CA Certificate for MQTT Protocol
const char* ca_cert_mqtt = \
"-----BEGIN CERTIFICATE-----\n" \
"MIIDTjCCAjagAwIBAgIJAJSa7eDdWLFUMA0GCSqGSIb3DQEBCwUAMDwxFDASBgNV\n" \
"BAMMCzE5Mi4xNjguNC4xMSQwIgYJKoZIhvcNAQkBFhVhdWxpYXdhcmRhbkBnbWFp\n" \
"bC5jb20wHhcNMTgwMjE4MTExNTU2WhcNMjgwMjE2MTExNTU2WjA8MRQwEgYDVQQD\n" \
"DAsxOTIuMTY4LjQuMTEkMCIGCSqGSIb3DQEJARYVYXVsaWF3YXJkYW5AZ21haWwu\n" \
"Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8LxiiyysS1dvWTs9\n" \
"K2AQ1fU0hyib5g0EqM+xwxgtfv6U1dI5ebF5yxyUStXIlMaWwpURuxYIrG5IQAw8\n" \
"15bIObiZmoxazfS4Je7OCDSA7tGUaUJRMV6RK7hev1S4GZ/8qcJjvdDxhbztGwpa\n" \
"gLnPtfAN0l+NLJm3tnPcFONUcOeMH7xp/jnxwsFUTEsutRmMhXhOC/Oc9ilMUhFl\n" \
"hA0Tz3wpFrDGlvrpm3w6+nk/6aYSeBSiEi9OpFCbOf/Nts7egc7CoTBCtmLeqEwn\n" \
"ScIQJzmkrewUFNWHBymRhvWTH0hyEbXmpn2M0FNFdBN9Bsoc/zDPQDlAVUA29VjQ\n" \
"DWFURwIDAQABo1MwUTAdBgNVHQ4EFgQU/RZVogaqJIoiGhSRPGpLmpbqfY4wHwYD\n" \
"VR0jBBgwFoAU/RZVogaqJIoiGhSRPGpLmpbqfY4wDwYDVR0TAQH/BAUwAwEB/zAN\n" \
"BgkqhkiG9w0BAQsFAAOCAQEAVFY5rZJPNY2fN0vbmVsxIX9zJBbd4HGjSDZqlbbQ\n" \
"Yt1wM5LSAKInvc+3m57tudUDLULFj8S17TNTEFzCB6Rpd5KUUwm3hjtrCQ6lbXub\n" \
"vZqab/tgXDwDv08cdcMzcbF5xcuKY0J3VgBZ+PQ+xi7b4KfNpNA2nIWO8IzFK8PC\n" \
"0kZDusl0wc0M4CNIJRjLKLoCFRE1EZR5QP/VrJR5rf4Z1Cep0iMmzMDOXk7LXi6W\n" \
"4lwvXyWeCr0b+Lt+oaINOCs213KYarhmp2A88XqRlJif5j/rSVx0Zwz4CLmT8Cxe\n" \
"tZ/YiktVGEXz/B3m3b5PsdpYP7/RSiTCrtoq3xn0O7VajA==\n" \
"-----END CERTIFICATE-----\n";

WiFiClientSecure espClient;
PubSubClient client_mqtt(espClient);

WiFiClientSecure client_http;

String getMacAddress() {
  uint8_t baseMac[6];
  esp_read_mac(baseMac, ESP_MAC_WIFI_STA);
  char baseMacChr[18] = {0};
  sprintf(baseMacChr, "%02X:%02X:%02X:%02X:%02X:%02X", baseMac[0], baseMac[1], baseMac[2], baseMac[3], baseMac[4], baseMac[5]);
  return String(baseMacChr);
}

void setup_wifi() {
  delay(100);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Mac address: ");
  Serial.println(getMacAddress());
}

void reconnect() {
  while (!client_mqtt.connected()) {
    Serial.print("Attempting MQTT connection...");
    clientId += String(random(0xffff), HEX);
    if (client_mqtt.connect(clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client_mqtt.state());
      Serial.println(" try again in 5 seconds");
      delay(6000);
    }
  }
}

void setup() {
  Serial.begin (115200);
  
  setup_wifi();
  
  espClient.setCACert(ca_cert_mqtt);
  client_http.setCACert(ca_cert_http);
  client_mqtt.setServer(server, 8883);
}

String HostIP = "Host: " + String(server);
String PostAPI = "POST https://" + String(server) + "/auth/ HTTP/1.1";
String PostData = "node_id=" + deviceId + "&node_pass=" + devicePass + "&node_mac=" + getMacAddress() + "&node_topic=" + topic;

void loop() {
  randNumber = random(20, 30);

  joinString = "{tp1: ";
  joinString = joinString + randNumber;
  joinString = joinString + "}";
  joinString = joinString + ", ";
  joinString = joinString + "{tp2: ";
  joinString = joinString + randNumber;
  joinString = joinString + "}";

  String sensing_data = joinString;
  
  SecurePublish(sensing_data);
  delay(600);
}

void SecurePublish(String data) {
  Serial.println("Connect to server HTTPS");
  if (client_http.connect(server, 443)){
      Serial.println("Connected to server!");
      client_http.println(PostAPI);
      Serial.println(PostAPI);
      client_http.println(HostIP);
      Serial.println(HostIP);
      client_http.println("Cache-Control: no-cache");
      client_http.println("Content-Type: application/x-www-form-urlencoded");
      client_http.print("Content-Length: ");
      client_http.println(PostData.length());
      client_http.println();
      client_http.println(PostData);
      Serial.println(PostData);

      Serial.print("Waiting for response ");
      while (!client_http.available()){
          delay(50);
          Serial.print(".");
      }

      String txtMsg = "";
      String token = "";
      int rsp = 0;
      
      while (client_http.available()) {
         char msg = client_http.read();
         if (msg != '\n') {
          if (msg != ' ') {
             txtMsg += msg;
          }
         }
      }
      
      rsp = txtMsg.length();
      token = txtMsg.substring(rsp - 16, rsp);
      Serial.println(token);

      CheckStatus(token, data);
      
      client_http.stop();
  } else {
      Serial.println("Connection failed!");
  }
}

void CheckStatus(String token, String data) {
  if (token == "gateway_not_registered") {
    Serial.println("Gateway Not Registered");
  } else if (token == "not_registered") {
    Serial.println("Node Not Registered");
  } else if (token == "") {
    Serial.println("Gateway Not Response");
  } else {
    if (!client_mqtt.connected()) {
      reconnect();
    }
    client_mqtt.loop();

    Serial.println(token);
    Serial.println(data);
    
    String jsonPayload="{\"Token\": \"";
    jsonPayload = jsonPayload + token;
    jsonPayload = jsonPayload + "\", \"Payload\": \"";
    jsonPayload = jsonPayload + data ;
    jsonPayload = jsonPayload + "\"}";

    int lenpayload = jsonPayload.length() + 5;
    
    char message[lenpayload];
    jsonPayload.toCharArray(message,lenpayload);
    
    delay(600);
    client_mqtt.publish(topic.c_str(), message);
    
    Serial.println(String(message));
  }
}
