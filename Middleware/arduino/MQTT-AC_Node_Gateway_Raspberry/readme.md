# Middleware Publisher (Prototype Library)


This application is security client tool for publisher in device node (ESP32-DevKitC). This application use to help device node to do authentication process and publish sensor data to device gateway (Raspberry Pi 3).


## Device Node Deployment Preparation
This research use ESP-DevKitC as device node. This middleware publisher will be deploy in ESP-DevKitC via USB cable using Arduino IDE. The requirement from the device node deployment is :
1. Download and install [Arduino IDE](https://www.arduino.cc/en/Main/Software "Arduino IDE").
2. Install and config ESP32 for Arduino Library via [this link](https://github.com/espressif/arduino-esp32 "ESP32 Arduino").
3. Install [PublishSubscribe Arduino Library](https://github.com/knolleary/pubsubclient "PublishSubscribe Arduino Library").


## Generate SSL Certificate
1. Log in to authentication server VPS via SSH.
2. Generate SSL Certificate using our script in `mqtt-ac/Tools/` folder.
3. For development in localhost you can use SSL Certificate in `master/Tools/ssl-cert` folder.


## Code Configuration
Please change the following variable in arduino file. The sample is like this :


  ``` bash
  const char* ssid = "gateway";
  const char* password = "rahasiatok";
  const char* server = "192.168.4.1";

  String deviceId = "node1";
  String devicePass = "48rssiq6";
  String topic = "confidential";
  String clientId = "mqtt-ac";
  ```


After that, open CA file from generated SSL certificate and copy the content to arduino file like this :


  ``` bash
  const char* ca_cert_http = \
  "-----BEGIN CERTIFICATE-----\n" \
  "MIIDTjCCAjagAwIBAgIJAJSa7eDdWLFUMA0GCSqGSIb3DQEBCwUAMDwxFDASBgNV\n" \
  "BAMMCzE5Mi4xNjguNC4xMSQwIgYJKoZIhvcNAQkBFhVhdWxpYXdhcmRhbkBnbWFp\n" \
  "bC5jb20wHhcNMTgwMjE4MTExNTU2WhcNMjgwMjE2MTExNTU2WjA8MRQwEgYDVQQD\n" \
  "DAsxOTIuMTY4LjQuMTEkMCIGCSqGSIb3DQEJARYVYXVsaWF3YXJkYW5AZ21haWwu\n" \
  "Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8LxiiyysS1dvWTs9\n" \
  "K2AQ1fU0hyib5g0EqM+xwxgtfv6U1dI5ebF5yxyUStXIlMaWwpURuxYIrG5IQAw8\n" \
  "15bIObiZmoxazfS4Je7OCDSA7tGUaUJRMV6RK7hev1S4GZ/8qcJjvdDxhbztGwpa\n" \
  "gLnPtfAN0l+NLJm3tnPcFONUcOeMH7xp/jnxwsFUTEsutRmMhXhOC/Oc9ilMUhFl\n" \
  "hA0Tz3wpFrDGlvrpm3w6+nk/6aYSeBSiEi9OpFCbOf/Nts7egc7CoTBCtmLeqEwn\n" \
  "ScIQJzmkrewUFNWHBymRhvWTH0hyEbXmpn2M0FNFdBN9Bsoc/zDPQDlAVUA29VjQ\n" \
  "DWFURwIDAQABo1MwUTAdBgNVHQ4EFgQU/RZVogaqJIoiGhSRPGpLmpbqfY4wHwYD\n" \
  "VR0jBBgwFoAU/RZVogaqJIoiGhSRPGpLmpbqfY4wDwYDVR0TAQH/BAUwAwEB/zAN\n" \
  "BgkqhkiG9w0BAQsFAAOCAQEAVFY5rZJPNY2fN0vbmVsxIX9zJBbd4HGjSDZqlbbQ\n" \
  "Yt1wM5LSAKInvc+3m57tudUDLULFj8S17TNTEFzCB6Rpd5KUUwm3hjtrCQ6lbXub\n" \
  "vZqab/tgXDwDv08cdcMzcbF5xcuKY0J3VgBZ+PQ+xi7b4KfNpNA2nIWO8IzFK8PC\n" \
  "0kZDusl0wc0M4CNIJRjLKLoCFRE1EZR5QP/VrJR5rf4Z1Cep0iMmzMDOXk7LXi6W\n" \
  "4lwvXyWeCr0b+Lt+oaINOCs213KYarhmp2A88XqRlJif5j/rSVx0Zwz4CLmT8Cxe\n" \
  "tZ/YiktVGEXz/B3m3b5PsdpYP7/RSiTCrtoq3xn0O7VajA==\n" \
  "-----END CERTIFICATE-----\n";

  const char* ca_cert_mqtt = \
  "-----BEGIN CERTIFICATE-----\n" \
  "MIIDTjCCAjagAwIBAgIJAJSa7eDdWLFUMA0GCSqGSIb3DQEBCwUAMDwxFDASBgNV\n" \
  "BAMMCzE5Mi4xNjguNC4xMSQwIgYJKoZIhvcNAQkBFhVhdWxpYXdhcmRhbkBnbWFp\n" \
  "bC5jb20wHhcNMTgwMjE4MTExNTU2WhcNMjgwMjE2MTExNTU2WjA8MRQwEgYDVQQD\n" \
  "DAsxOTIuMTY4LjQuMTEkMCIGCSqGSIb3DQEJARYVYXVsaWF3YXJkYW5AZ21haWwu\n" \
  "Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8LxiiyysS1dvWTs9\n" \
  "K2AQ1fU0hyib5g0EqM+xwxgtfv6U1dI5ebF5yxyUStXIlMaWwpURuxYIrG5IQAw8\n" \
  "15bIObiZmoxazfS4Je7OCDSA7tGUaUJRMV6RK7hev1S4GZ/8qcJjvdDxhbztGwpa\n" \
  "gLnPtfAN0l+NLJm3tnPcFONUcOeMH7xp/jnxwsFUTEsutRmMhXhOC/Oc9ilMUhFl\n" \
  "hA0Tz3wpFrDGlvrpm3w6+nk/6aYSeBSiEi9OpFCbOf/Nts7egc7CoTBCtmLeqEwn\n" \
  "ScIQJzmkrewUFNWHBymRhvWTH0hyEbXmpn2M0FNFdBN9Bsoc/zDPQDlAVUA29VjQ\n" \
  "DWFURwIDAQABo1MwUTAdBgNVHQ4EFgQU/RZVogaqJIoiGhSRPGpLmpbqfY4wHwYD\n" \
  "VR0jBBgwFoAU/RZVogaqJIoiGhSRPGpLmpbqfY4wDwYDVR0TAQH/BAUwAwEB/zAN\n" \
  "BgkqhkiG9w0BAQsFAAOCAQEAVFY5rZJPNY2fN0vbmVsxIX9zJBbd4HGjSDZqlbbQ\n" \
  "Yt1wM5LSAKInvc+3m57tudUDLULFj8S17TNTEFzCB6Rpd5KUUwm3hjtrCQ6lbXub\n" \
  "vZqab/tgXDwDv08cdcMzcbF5xcuKY0J3VgBZ+PQ+xi7b4KfNpNA2nIWO8IzFK8PC\n" \
  "0kZDusl0wc0M4CNIJRjLKLoCFRE1EZR5QP/VrJR5rf4Z1Cep0iMmzMDOXk7LXi6W\n" \
  "4lwvXyWeCr0b+Lt+oaINOCs213KYarhmp2A88XqRlJif5j/rSVx0Zwz4CLmT8Cxe\n" \
  "tZ/YiktVGEXz/B3m3b5PsdpYP7/RSiTCrtoq3xn0O7VajA==\n" \
  "-----END CERTIFICATE-----\n";
  ```


When all configuration is ready, you can upload the program to ESP32-DevKitC via Arduino IDE.
