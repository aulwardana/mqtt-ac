/*
  mqtt-ac.h - Library for secure publish in MQTT Protcol.
  Created by Aulia Arif Wardana, April 16, 2018.
  Released into the public domain.
*/

#ifndef MqttAC_h
#define MqttAC_h

#include "Arduino.h"
#include "WiFiClientSecure.h"
#include "PubSubClient.h"
#include "esp_system.h"

class MqttAC
{
  public:
    void SetDeviceID(String deviceId);
    void SetPassword(String devicePass);
    void SetTopic(String topic);
    void SetClientID(String clientId);
    void SetWifi(const char* ssid, const char* password);
    void SetGatewayIP(const char* gatewayIP);
    void SetCAauth(const char* ca_cert_http);
    void SetCApub(const char* ca_cert_mqtt);

    void SecPublish(String message);
};

#endif