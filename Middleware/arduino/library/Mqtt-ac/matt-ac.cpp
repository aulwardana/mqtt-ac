/*
  mqtt-ac.cpp - Library for secure publish in MQTT Protcol.
  Created by Aulia Arif Wardana, April 16, 2018.
  Released into the public domain.
*/

#include "Arduino.h"
#include "mqtt-ac.h"
#include "WiFiClientSecure.h"
#include "PubSubClient.h"
#include "esp_system.h"

WiFiClientSecure espClient;
PubSubClient client_mqtt(espClient);

WiFiClientSecure client_http;

String _deviceId;
String _devicePass;
String _topic;
String _clientId;

const char* _gatewayIP;
const char* _ca_cert_http;
const char* _ca_cert_mqtt;

String _HostIP;
String _PostAPI;
String _PostData;

String getMacAddress() {
  uint8_t baseMac[6];
  esp_read_mac(baseMac, ESP_MAC_WIFI_STA);
  char baseMacChr[18] = {0};
  sprintf(baseMacChr, "%02X:%02X:%02X:%02X:%02X:%02X", baseMac[0], baseMac[1], baseMac[2], baseMac[3], baseMac[4], baseMac[5]);
  return String(baseMacChr);
}

void MqttAC::SetDeviceID(String deviceId)
{
  _deviceId = deviceId;
}

void MqttAC::SetPassword(String devicePass)
{
  _devicePass = devicePass;
}

void MqttAC::SetTopic(String topic)
{
  _topic = topic;
}

void MqttAC::SetClientID(String clientId)
{
  _clientId = clientId;
}

void MqttAC::SetGatewayIP(const char* gatewayIP)
{
  _gatewayIP = gatewayIP;
  client_mqtt.setServer(_gatewayIP, 8883);
}

void MqttAC::SetCAauth(const char* ca_cert_http)
{
  _ca_cert_http = ca_cert_http;
  client_http.setCACert(_ca_cert_http);
}

void MqttAC::SetCApub(const char* ca_cert_mqtt)
{
  _ca_cert_mqtt = ca_cert_mqtt;
  espClient.setCACert(_ca_cert_mqtt);
}

void MqttAC::SetWifi(const char* ssid, const char* password) {
  delay(100);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Mac address: ");
  Serial.println(getMacAddress());
}

void reconnect() {
  while (!client_mqtt.connected()) {
    Serial.print("Attempting MQTT connection...");
    _clientId += String(random(0xffff), HEX);
    if (client_mqtt.connect(_clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client_mqtt.state());
      Serial.println(" try again in 5 seconds");
      delay(6000);
    }
  }
}

void CheckStatus(String token, String msg) {
  if (token == "gateway_not_registered") {
    Serial.println("Gateway Not Registered");
  } else if (token == "not_registered") {
    Serial.println("Node Not Registered");
  } else if (token == "") {
    Serial.println("Gateway Not Response");
  } else {
    if (!client_mqtt.connected()) {
      reconnect();
    }
    client_mqtt.loop();
    
    String jsonPayload="{\"Token\": \"";
    jsonPayload = jsonPayload + token;
    jsonPayload = jsonPayload + "\", \"Payload\": \"";
    jsonPayload = jsonPayload + msg;
    jsonPayload = jsonPayload + "\"}";

    int lenpayload = jsonPayload.length() + 5;
    
    char message[lenpayload];
    jsonPayload.toCharArray(message,lenpayload);
    
    delay(600);
    client_mqtt.publish(_topic.c_str(), message);
    
    Serial.println(String(message));
  }
}

void MqttAC::SecPublish(String message) {
  _HostIP = "Host: " + String(_gatewayIP);
  _PostAPI = "POST https://" + String(_gatewayIP) + "/auth/ HTTP/1.1";
  _PostData = "node_id=" + _deviceId + "&node_pass=" + _devicePass + "&node_mac=" + getMacAddress() + "&node_topic=" + _topic;
  Serial.println("Connect to server HTTPS");
  if (client_http.connect(_gatewayIP, 443)){
      Serial.println("Connected to server!");
      client_http.println(_PostAPI);
      Serial.println(_PostAPI);
      client_http.println(_HostIP);
      Serial.println(_HostIP);
      client_http.println("Cache-Control: no-cache");
      client_http.println("Content-Type: application/x-www-form-urlencoded");
      client_http.print("Content-Length: ");
      client_http.println(_PostData.length());
      client_http.println();
      client_http.println(_PostData);
      Serial.println(_PostData);

      Serial.print("Waiting for response ");
      while (!client_http.available()){
          delay(50);
          Serial.print(".");
      }

      String txtMsg = "";
      String token = "";
      int rsp = 0;
      
      while (client_http.available()) {
         char msg = client_http.read();
         if (msg != '\n') {
          if (msg != ' ') {
             txtMsg += msg;
          }
         }
      }
      
      rsp = txtMsg.length();
      token = txtMsg.substring(rsp - 16, rsp);
      Serial.println(token);

      CheckStatus(token, message);
      
      client_http.stop();
  } else {
      Serial.println("Connection failed!");
  }
}

