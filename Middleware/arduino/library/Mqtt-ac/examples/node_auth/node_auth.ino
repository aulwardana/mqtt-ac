#include <mqtt-ac.h>

MqttAC mqttAC;

const char* ssid = ""; //Gateway WiFi SSID Name
const char* password = ""; //Gateway WiFi Password
const char* server = ""; //Gateway WiFi Server IP

String deviceId = ""; //Registered Device ID (Device Node) in Authentication Server
String devicePass = ""; //Registered Device Password (Device Node) in Authentication Server
String topic = ""; //Registered MQTT Topic (Device Node to Device Gateway) in Authentication Server
String clientId = ""; //Client ID for MQTT connection between Device Node to Device Gateway

long randNumber;
String joinString;

//You can change with your own CA Certificate for HTTP Protocol
const char* ca_cert_http = \
"-----BEGIN CERTIFICATE-----\n" \
"MIIDTjCCAjagAwIBAgIJAJSa7eDdWLFUMA0GCSqGSIb3DQEBCwUAMDwxFDASBgNV\n" \
"BAMMCzE5Mi4xNjguNC4xMSQwIgYJKoZIhvcNAQkBFhVhdWxpYXdhcmRhbkBnbWFp\n" \
"bC5jb20wHhcNMTgwMjE4MTExNTU2WhcNMjgwMjE2MTExNTU2WjA8MRQwEgYDVQQD\n" \
"DAsxOTIuMTY4LjQuMTEkMCIGCSqGSIb3DQEJARYVYXVsaWF3YXJkYW5AZ21haWwu\n" \
"Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8LxiiyysS1dvWTs9\n" \
"K2AQ1fU0hyib5g0EqM+xwxgtfv6U1dI5ebF5yxyUStXIlMaWwpURuxYIrG5IQAw8\n" \
"15bIObiZmoxazfS4Je7OCDSA7tGUaUJRMV6RK7hev1S4GZ/8qcJjvdDxhbztGwpa\n" \
"gLnPtfAN0l+NLJm3tnPcFONUcOeMH7xp/jnxwsFUTEsutRmMhXhOC/Oc9ilMUhFl\n" \
"hA0Tz3wpFrDGlvrpm3w6+nk/6aYSeBSiEi9OpFCbOf/Nts7egc7CoTBCtmLeqEwn\n" \
"ScIQJzmkrewUFNWHBymRhvWTH0hyEbXmpn2M0FNFdBN9Bsoc/zDPQDlAVUA29VjQ\n" \
"DWFURwIDAQABo1MwUTAdBgNVHQ4EFgQU/RZVogaqJIoiGhSRPGpLmpbqfY4wHwYD\n" \
"VR0jBBgwFoAU/RZVogaqJIoiGhSRPGpLmpbqfY4wDwYDVR0TAQH/BAUwAwEB/zAN\n" \
"BgkqhkiG9w0BAQsFAAOCAQEAVFY5rZJPNY2fN0vbmVsxIX9zJBbd4HGjSDZqlbbQ\n" \
"Yt1wM5LSAKInvc+3m57tudUDLULFj8S17TNTEFzCB6Rpd5KUUwm3hjtrCQ6lbXub\n" \
"vZqab/tgXDwDv08cdcMzcbF5xcuKY0J3VgBZ+PQ+xi7b4KfNpNA2nIWO8IzFK8PC\n" \
"0kZDusl0wc0M4CNIJRjLKLoCFRE1EZR5QP/VrJR5rf4Z1Cep0iMmzMDOXk7LXi6W\n" \
"4lwvXyWeCr0b+Lt+oaINOCs213KYarhmp2A88XqRlJif5j/rSVx0Zwz4CLmT8Cxe\n" \
"tZ/YiktVGEXz/B3m3b5PsdpYP7/RSiTCrtoq3xn0O7VajA==\n" \
"-----END CERTIFICATE-----\n";

//You can change with your own CA Certificate for MQTT Protocol
const char* ca_cert_mqtt = \
"-----BEGIN CERTIFICATE-----\n" \
"MIIDTjCCAjagAwIBAgIJAJSa7eDdWLFUMA0GCSqGSIb3DQEBCwUAMDwxFDASBgNV\n" \
"BAMMCzE5Mi4xNjguNC4xMSQwIgYJKoZIhvcNAQkBFhVhdWxpYXdhcmRhbkBnbWFp\n" \
"bC5jb20wHhcNMTgwMjE4MTExNTU2WhcNMjgwMjE2MTExNTU2WjA8MRQwEgYDVQQD\n" \
"DAsxOTIuMTY4LjQuMTEkMCIGCSqGSIb3DQEJARYVYXVsaWF3YXJkYW5AZ21haWwu\n" \
"Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8LxiiyysS1dvWTs9\n" \
"K2AQ1fU0hyib5g0EqM+xwxgtfv6U1dI5ebF5yxyUStXIlMaWwpURuxYIrG5IQAw8\n" \
"15bIObiZmoxazfS4Je7OCDSA7tGUaUJRMV6RK7hev1S4GZ/8qcJjvdDxhbztGwpa\n" \
"gLnPtfAN0l+NLJm3tnPcFONUcOeMH7xp/jnxwsFUTEsutRmMhXhOC/Oc9ilMUhFl\n" \
"hA0Tz3wpFrDGlvrpm3w6+nk/6aYSeBSiEi9OpFCbOf/Nts7egc7CoTBCtmLeqEwn\n" \
"ScIQJzmkrewUFNWHBymRhvWTH0hyEbXmpn2M0FNFdBN9Bsoc/zDPQDlAVUA29VjQ\n" \
"DWFURwIDAQABo1MwUTAdBgNVHQ4EFgQU/RZVogaqJIoiGhSRPGpLmpbqfY4wHwYD\n" \
"VR0jBBgwFoAU/RZVogaqJIoiGhSRPGpLmpbqfY4wDwYDVR0TAQH/BAUwAwEB/zAN\n" \
"BgkqhkiG9w0BAQsFAAOCAQEAVFY5rZJPNY2fN0vbmVsxIX9zJBbd4HGjSDZqlbbQ\n" \
"Yt1wM5LSAKInvc+3m57tudUDLULFj8S17TNTEFzCB6Rpd5KUUwm3hjtrCQ6lbXub\n" \
"vZqab/tgXDwDv08cdcMzcbF5xcuKY0J3VgBZ+PQ+xi7b4KfNpNA2nIWO8IzFK8PC\n" \
"0kZDusl0wc0M4CNIJRjLKLoCFRE1EZR5QP/VrJR5rf4Z1Cep0iMmzMDOXk7LXi6W\n" \
"4lwvXyWeCr0b+Lt+oaINOCs213KYarhmp2A88XqRlJif5j/rSVx0Zwz4CLmT8Cxe\n" \
"tZ/YiktVGEXz/B3m3b5PsdpYP7/RSiTCrtoq3xn0O7VajA==\n" \
"-----END CERTIFICATE-----\n";

void setup(){
    Serial.begin(115200);

    mqttAC.SetWifi(ssid, password);
    mqttAC.SetGatewayIP(server);
    mqttAC.SetDeviceID(deviceId);
    mqttAC.SetPassword(devicePass);
    mqttAC.SetTopic(topic);
    mqttAC.SetClientID(clientId);
    mqttAC.SetCAauth(ca_cert_http);
    mqttAC.SetCApub(ca_cert_mqtt);
}

void loop() {
    randNumber = random(20, 30);

    joinString = "sample: ";
    joinString = joinString + randNumber;

    String sensing_data = joinString;

    mqttAC.SecPublish(sensing_data);
    delay(600);
}
