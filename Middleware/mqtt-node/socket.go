package main

import (
	"bytes"
	"fmt"
	"net"
)

var (
	hwaddr string
	nmaddr string
)

func SocketInfo() {
	getSocketInfo()

	hwaddrInfo := fmt.Sprintf("Mac Address : %s", hwaddr)
	nmaddrInfo := fmt.Sprintf("Adapter : %s", nmaddr)

	LogInfo(hwaddrInfo)
	LogInfo(nmaddrInfo)
}

func getSocketInfo() {
	interfaces, err := net.Interfaces()
	if err == nil {
		for _, i := range interfaces {
			if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {
				hwaddr = i.HardwareAddr.String()
				nmaddr = i.Name
				break
			}
		}
	}
}
