package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"runtime"
	"sync"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/gorilla/mux"
	"github.com/op/go-logging"
)

var (
	cnf        *Config
	configPath string
)

type SecurePayload struct {
	Token   string
	Payload string
}

var log = logging.MustGetLogger("secure-log-style")

var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:05x}%{color:reset} %{message}`,
)

/*
Options:
 [-help]                      Display help
 [-v]                         Display detail logging (verbose mode)
 [-u http|mqtt]               Action http (update http ssl) or mqtt (update mqtt ssl)
 [-c <path/config.yaml>]      Use custom configuration file (config.yaml)
*/

func initConfig() error {
	flag.StringVar(&configPath, "c", "config-node.yaml", "Configuration File")

	c, err := NewCfg(configPath)
	if err != nil {
		return err
	}
	cnf = c

	return err
}

var f MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	var m SecurePayload

	if err := json.Unmarshal(msg.Payload(), &m); err != nil {
		log.Critical(err)
	}

	LogInfo(m.Token)

	if verifyToken(m.Token) == true {
		LogPayload(msg.Topic(), m.Payload)
	} else {
		log.Notice("Token Invalid")
	}
}

func main() {
	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend2 := logging.NewLogBackend(os.Stderr, "", 0)

	backend2Formatter := logging.NewBackendFormatter(backend2, format)
	backend1Leveled := logging.AddModuleLevel(backend1)
	backend1Leveled.SetLevel(logging.ERROR, "")

	logging.SetBackend(backend1Leveled, backend2Formatter)

	err := initConfig()
	if err != nil {
		log.Fatal(err)
	}

	update := flag.String("u", "", "Action http (update http ssl) or mqtt (update mqtt ssl)")
	verbosLogeMode := flag.Bool("v", false, "Used to detail logging (verbose mode)")
	flag.Parse()

	LogStatus(*verbosLogeMode)

	AESInit()

	InitServerSSl()

	SocketInfo()

	confKDM()

	conKDM()

	tlsconfig := NewTLSConfig()

	hostMQTT := fmt.Sprintf("%s://%s:%d", cnf.MqttCfg().Protocol, cnf.MqttCfg().Address, cnf.MqttCfg().Port)

	opts := MQTT.NewClientOptions().AddBroker(hostMQTT)
	opts.SetClientID(cnf.MqttCfg().ClientID).SetTLSConfig(tlsconfig)
	opts.SetDefaultPublishHandler(f)

	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		log.Error(token.Error())
	}

	r := mux.NewRouter()

	Route(r, "/", MainHome, GET)
	Route(r, "/auth/", AuthAPI, POST)

	if *update == "http" {
		UpdateServerHttpSSL()
	} else if *update == "mqtt" {
		UpdateServerMqttSSL()
	} else {
		LogInfo("Invalid setting for -update ssl, must be http or mqtt")
	}

	if CRTCheck() == true {
		runtime.GOMAXPROCS(1)
		var wg sync.WaitGroup
		wg.Add(3)

		go func() {
			defer wg.Done()
			mqttPortDebug := fmt.Sprintf("Subscribe Secure-MQTT Start on: %s", hostMQTT)
			mqttTopicDebug := fmt.Sprintf("Topic: %s", cnf.MqttCfg().Topic)
			LogDebug(mqttPortDebug)
			LogDebug(mqttTopicDebug)
			if token := c.Subscribe(filterTopic(cnf.MqttCfg().Topic), 0, nil); token.Wait() && token.Error() != nil {
				log.Error(token.Error())
				os.Exit(1)
			}
		}()

		go func() {
			defer wg.Done()
			webPortDebug := fmt.Sprintf("Web Service start at port: %s", cnf.HttpCfg().Port)
			LogDebug(webPortDebug)
			Run(r, cnf.HttpCfg().Address, cnf.HttpCfg().Port)
		}()

		wg.Wait()
	} else {
		log.Info("Please run mqtt-ac first to setup gateway")
	}

}
