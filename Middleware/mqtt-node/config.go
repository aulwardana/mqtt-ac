package main

import (
	"bytes"
	"io/ioutil"
	"os"

	"github.com/spf13/viper"
)

const (
	MQTT_PROTOCOL          string = "mqtt.protocol"
	MQTT_ADDRESS           string = "mqtt.address"
	MQTT_PORT              string = "mqtt.port"
	MQTT_TOPIC             string = "mqtt.topic"
	MQTT_CLIENT_ID         string = "mqtt.clientid"
	MQTT_SERVER_CAFILEDIR  string = "mqtt.srv/cafiledir"
	MQTT_SERVER_CRTFILEDIR string = "mqtt.srv/crtfiledir"
	MQTT_SERVER_KEYFILEDIR string = "mqtt.srv/keyfiledir"
	HTTP_PROTOCOL          string = "http.protocol"
	HTTP_ADDRESS           string = "http.address"
	HTTP_PORT              string = "http.port"
	HTTP_SERVER_CAFILEDIR  string = "http.srv/cafiledir"
	HTTP_SERVER_CRTFILEDIR string = "http.srv/crtfiledir"
	HTTP_SERVER_KEYFILEDIR string = "http.srv/keyfiledir"
	DEVICE_ID              string = "device.id"
	AUTH_URL               string = "device.auth/url"
	AUTH_PROTOCOL          string = "device.auth/protocol"
	AUTH_PORT              string = "device.auth/port"
)

type MqttConfig struct {
	Protocol      string
	Address       string
	Port          int
	Topic         string
	ClientID      string
	SrvCAFileDir  string
	SrvCrtFileDir string
	SrvKeyFileDir string
}

type HttpConfig struct {
	Protocol      string
	Address       string
	Port          int
	SrvCAFileDir  string
	SrvCrtFileDir string
	SrvKeyFileDir string
}

type DeviceConfig struct {
	DeviceID     string
	AuthUrl      string
	AuthProtocol string
	AuthPort     int
}

type Config struct {
	*viper.Viper
}

func setDefaults(v *viper.Viper) {
	v.SetDefault(MQTT_PROTOCOL, "tcp")
	v.SetDefault(MQTT_ADDRESS, "localhost")
	v.SetDefault(MQTT_PORT, 1883)
	v.SetDefault(MQTT_TOPIC, "cofidential")
	v.SetDefault(MQTT_CLIENT_ID, "mqtt-ac")
	v.SetDefault(MQTT_SERVER_CAFILEDIR, "ssl-cert-gateway/mqtt/server-ca.crt")
	v.SetDefault(MQTT_SERVER_CRTFILEDIR, "ssl-cert-gateway/mqtt/server-crt.crt")
	v.SetDefault(MQTT_SERVER_KEYFILEDIR, "ssl-cert-gateway/mqtt/server-key.key")
	v.SetDefault(HTTP_PROTOCOL, "http")
	v.SetDefault(HTTP_ADDRESS, "localhost")
	v.SetDefault(HTTP_PORT, 8443)
	v.SetDefault(HTTP_SERVER_CAFILEDIR, "ssl-cert-gateway/http/server-ca.crt")
	v.SetDefault(HTTP_SERVER_CRTFILEDIR, "ssl-cert-gateway/http/server-crt.crt")
	v.SetDefault(HTTP_SERVER_KEYFILEDIR, "ssl-cert-gateway/http/server-key.key")
	v.SetDefault(DEVICE_ID, " ")
	v.SetDefault(AUTH_URL, "localhost")
	v.SetDefault(AUTH_PROTOCOL, "http")
	v.SetDefault(AUTH_PORT, 8443)
}

func loadConfPath(v *viper.Viper, path string) error {
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	f, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	return v.ReadConfig(bytes.NewBuffer(f))
}

func (c *Config) MqttCfg() *MqttConfig {
	return &MqttConfig{
		Protocol:      c.GetString(MQTT_PROTOCOL),
		Address:       c.GetString(MQTT_ADDRESS),
		Port:          c.GetInt(MQTT_PORT),
		Topic:         c.GetString(MQTT_TOPIC),
		ClientID:      c.GetString(MQTT_CLIENT_ID),
		SrvCAFileDir:  c.GetString(MQTT_SERVER_CAFILEDIR),
		SrvCrtFileDir: c.GetString(MQTT_SERVER_CRTFILEDIR),
		SrvKeyFileDir: c.GetString(MQTT_SERVER_KEYFILEDIR),
	}
}

func (c *Config) HttpCfg() *HttpConfig {
	return &HttpConfig{
		Protocol:      c.GetString(HTTP_PROTOCOL),
		Address:       c.GetString(HTTP_ADDRESS),
		Port:          c.GetInt(HTTP_PORT),
		SrvCAFileDir:  c.GetString(HTTP_SERVER_CAFILEDIR),
		SrvCrtFileDir: c.GetString(HTTP_SERVER_CRTFILEDIR),
		SrvKeyFileDir: c.GetString(HTTP_SERVER_KEYFILEDIR),
	}
}

func (c *Config) DeviceCfg() *DeviceConfig {
	return &DeviceConfig{
		DeviceID:     c.GetString(DEVICE_ID),
		AuthUrl:      c.GetString(AUTH_URL),
		AuthProtocol: c.GetString(AUTH_PROTOCOL),
		AuthPort:     c.GetInt(AUTH_PORT),
	}
}

func NewCfg(path ...string) (*Config, error) {
	v := viper.New()
	v.SetConfigType("yaml")
	setDefaults(v)

	if len(path) > 0 {
		if err := loadConfPath(v, path[0]); err != nil {
			log.Warning("Failed load from file. Mqtt-node using default configuration")
		}
	}

	return &Config{v}, nil
}
