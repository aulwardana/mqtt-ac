package main

import (
	"crypto/aes"
	"crypto/cipher"
	"io/ioutil"
	"os"
)

const (
	crtFolderPath  = "./.crt/"
	credentialPath = "./.crt/identity.aul"
	AESKeyPath     = "./.crt/aes.aul"
)

var (
	AESKey string
)

func AESInit() {
	if _, err := os.Stat(crtFolderPath); err != nil {
		if os.IsNotExist(err) {
			log.Error("Credential Folder Not Found : Please start mqtt-ac middleware first")
		}
	}

	if _, err := os.Stat(credentialPath); err != nil {
		if os.IsNotExist(err) {
			log.Error("Credential File Not Found : Please start mqtt-ac middleware first")
		}
	}

	if _, err := os.Stat(AESKeyPath); err != nil {
		if os.IsNotExist(err) {
			log.Error("AES Key Not Found : Please start mqtt-ac middleware first")
		}
	} else {
		AESKeyGen()
	}
}

func CRTCheck() bool {
	if CRTFile() == true {
		LogInfo("Registered")
		return true
	} else {
		LogInfo("Not Registered : Please start mqtt-ac middleware first")
		return false
	}

	return false
}

func CRTRead() (string, error) {
	data, err := ioutil.ReadFile(credentialPath)
	credentialStr := DecryptSTR(string(data), AESKey)
	return credentialStr, err
}

func CRTFile() bool {
	data, err := ioutil.ReadFile(credentialPath)
	if err != nil {
		log.Error(err)
		return true
	}

	if string(data) != "" {
		return true
	} else {
		return false
	}

	return false
}

func AESKeyGen() {
	data, err := ioutil.ReadFile(AESKeyPath)
	if err != nil {
		log.Critical(err)
	}
	AESKey = string(data)
}

func DecryptSTR(cipherstring string, keystring string) string {
	ciphertext := []byte(cipherstring)
	AESKey := []byte(keystring)

	block, err := aes.NewCipher(AESKey)
	if err != nil {
		log.Critical(err)
	}

	if len(ciphertext) < aes.BlockSize {
		log.Error("Text is too short")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)

	return string(ciphertext)
}
