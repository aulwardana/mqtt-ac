# Middleware Subscriber and Node Key Manager (NKM) App


This application is security client tool for device gateway (Raspberry Pi) subscriber and  management tool for authentication process from device node (ESP32-DevKitC). This application use to help device gateway to manage device node authentication process in cloud server. After the device node successfully authenticate, the data is also subscribed by this application.


## Raspbery Pi 3 Preparation
This research use Raspbery Pi 3 Model B as device gateway. The NKM app will be deploy in raspbery pi device. The requirement from the raspbery is :
1. The raspbery pi must use [Raspbian OS](https://www.raspberrypi.org/downloads/raspbian/ "RASPBIAN STRETCH WITH DESKTOP") for the operating system.
2. After that the SSH option from raspbian os must active to remote the raspbery pi.
3. Install [MQTT Mosquitto](http://www.switchdoc.com/2016/02/tutorial-installing-and-testing-mosquitto-mqtt-on-raspberry-pi/ "MQTT Mosquitto") in raspbian os.
4. Configure Raspberry Pi 3 as access point, follow [this link](https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md "Raspberry Pi 3 Access Point").


## Generate SSL Certificate
1. Log in to authentication server VPS via SSH.
2. Generate SSL Certificate using our script in `mqtt-ac/Tools/` folder.
3. For development in localhost you can use SSL Certificate in `master/Tools/ssl-cert` folder.


## Configure Secure Broker in Device Gateway (Raspberry Pi 3)
First to configure the secure broker please follow [this link](https://mcuoneclipse.com/2017/04/14/enable-secure-communication-with-tls-and-the-mosquitto-broker/ "MQTT Broker SSL/TLS").


## Config File NKM App
Please configure the config file `config-sample.yaml` in `mqtt-ac/Middleware/mqtt-node` folder, sample configuration is like this :


  ``` bash
  mqtt: 
    protocol: "ssl"
    address: "192.168.4.1"
    port: 8883 
  topic: "confidential"
    clientid: "mqtt-ac"  
    srv/cafiledir: "ssl-cert-gateway/mqtt/ca-bc.crt" 
    srv/crtfiledir: "ssl-cert-gateway/mqtt/client-bc.crt"
    srv/keyfiledir: "ssl-cert-gateway/mqtt/client-bc.key"
  http: 
    protocol: "https"
    address: "192.168.4.1"
    port: 443
    srv/cafiledir: "ssl-cert-gateway/http/ca-bc.crt"
    srv/crtfiledir: "ssl-cert-gateway/http/server-bc.crt" 
    srv/keyfiledir: "ssl-cert-gateway/http/server-bc.key"
  device:
    id: "device-rsp"
    auth/url: "104.154.195.155"  
    auth/protocol: "https"
    auth/port: 443
  ```


After configuration finish, please rename this configuration with `config-node.yaml` and add this configuration in same folder with NKM app.


## Build NKM App for Raspberry
For NKM App, you must build with special command like this :


  ``` bash
  GOARM=7 GOOS=linux GOARCH=arm go build main.go config.go handler.go sec-cert.go socket.go ssl.go token.go
  ```


The build command is special because the NKM app will deploy in raspbery pi that use ARM processor to running the app. We must build the app with defined specific operating system and processor architecture. Rename binary app with `node` and place in same folder with configuration file. Then, use this command to allow application to use port 443.


  ``` bash
  sudo setcap CAP_NET_BIND_SERVICE=+eip ~/mqtt-kdm
  ```


When NKM App running, it will be listen the conection between publisher and subscriber that connect to device gateway.


## NKM App Options
You can use help command to show all options from this app, the command is : 


  ``` bash
  $ ./node --help
  ```


it will be show like this :


  ``` bash
  Options:
  [-help]                      Display help
  [-u http|mqtt]               Action http (update http ssl) or mqtt (update mqtt ssl)
  [-c <path/config.yaml>]      Use custom configuration file (config.yaml)
  ```


The SSL certificate is updateable, you can copy the update file from SSL certificate to SSL folder in configuration file and run update command.