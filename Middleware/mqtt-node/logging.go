package main

import (
	"fmt"
)

var (
	verboseLog bool
)

func LogStatus(verbosLogeMode bool) {
	if verbosLogeMode == true {
		verboseLog = verbosLogeMode
		LogInfo("Verbose Log : On")
	} else {
		verboseLog = verbosLogeMode
	}
}

func LogInfo(s string) {
	if verboseLog == true {
		log.Info(s)
	}
}

func LogDebug(s string) {
	if verboseLog == true {
		log.Debug(s)
	}
}

func LogPayload(topic string, msg string) {
	if verboseLog == true {
		log.Notice("Topic:", topic, "Msg:", msg)
	} else {
		fmt.Println(msg)
	}
}
