package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/gorilla/mux"
)

var (
	dumpToken string
)

const (
	GET    string = "GET"
	POST   string = "POST"
	PUT    string = "PUT"
	DELETE string = "DELETE"
)

func Route(r *mux.Router, path string, handler func(http.ResponseWriter, *http.Request), method string) {
	r.HandleFunc(path, handler).Methods(method)
}

func Run(r *mux.Router, address string, port int) {
	caCertPool := x509.NewCertPool()

	caCert, err := DencryptSSl(SecSslHttpServerCA)
	if err != nil {
		log.Fatal(err)
	}

	caCertPool.AppendCertsFromPEM(caCert)
	cfg := &tls.Config{
		ClientCAs: caCertPool,
	}

	portHttps := fmt.Sprintf("%s:%d", address, port)
	srv := &http.Server{
		Addr:      portHttps,
		Handler:   r,
		TLSConfig: cfg,
	}

	serverInfoRun := fmt.Sprint("Gateway Middleware Started: %s", srv.Addr)
	LogDebug(serverInfoRun)

	TempServerSSl()

	srv.ListenAndServeTLS(TempSecSslHttpServerCert, TempSecSslHttpServerKey)

	os.Remove(TempSecSslHttpServerCert)
	os.Remove(TempSecSslHttpServerKey)
}

func confKDM() (session *http.Client) {
	caCertPool := x509.NewCertPool()

	caCert, err := DencryptSSl(SecSslHttpCert)
	if err != nil {
		log.Critical(err)
	}

	caCertPool.AppendCertsFromPEM(caCert)

	cert, err := LoadX509KeyPairDec(SecSslHttpClientCert, SecSslHttpClientKey)
	if err != nil {
		log.Critical(err)
	}

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      caCertPool,
				Certificates: []tls.Certificate{cert},
			},
		},
	}

	return client
}

func conKDM() {
	client := confKDM()

	urlHttp := fmt.Sprintf("%s://%s:%d", cnf.DeviceCfg().AuthProtocol, cnf.DeviceCfg().AuthUrl, cnf.DeviceCfg().AuthPort)
	resp, err := client.Get(urlHttp)
	if err != nil {
		log.Error(err)
		return
	}

	htmlData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error(err)
		return
	}
	defer resp.Body.Close()

	conKDMResp := fmt.Sprintf("response : %s", resp.Status)
	conKDMMsg := fmt.Sprint("message : %s", string(htmlData))

	LogInfo(conKDMResp)
	LogInfo(conKDMMsg)
}

func MainHome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Connected to Gateway\n"))
	LogInfo("a device has been connected to gateway")
}

func AuthAPI(w http.ResponseWriter, r *http.Request) {
	AESKeyGen()
	PassRead, err := CRTRead()
	if err != nil {
		log.Error(err)
	}

	NodeId := r.FormValue("node_id")
	NodePass := r.FormValue("node_pass")
	NodeMac := r.FormValue("node_mac")
	NodeTopic := r.FormValue("node_topic")

	v := url.Values{}

	v.Set("dev_id", cnf.DeviceCfg().DeviceID)
	v.Set("dev_pass", PassRead)
	v.Set("dev_mac", hwaddr)
	v.Set("dev_topic", cnf.MqttCfg().Topic)

	v.Set("node_id", NodeId)
	v.Set("node_pass", NodePass)
	v.Set("node_mac", NodeMac)
	v.Set("node_topic", NodeTopic)

	s := v.Encode()

	client := confKDM()

	reqAuthUrl := fmt.Sprintf("%s://%s:%d/node-auth/", cnf.DeviceCfg().AuthProtocol, cnf.DeviceCfg().AuthUrl, cnf.DeviceCfg().AuthPort)
	reqAuth, err := client.Post(reqAuthUrl, "application/x-www-form-urlencoded", strings.NewReader(s))
	if err != nil {
		log.Error(err)
	}

	getStatusNode, err := ioutil.ReadAll(reqAuth.Body)
	if err != nil {
		log.Error(err)
	}
	defer reqAuth.Body.Close()

	statusGateway := fmt.Sprintf("response : %s", reqAuth.Status)

	if string(getStatusNode) == "gateway_not_registered" {
		LogInfo(statusGateway)
		LogInfo("register : gateway not registered")

		w.Write([]byte("gateway_not_registered"))
	} else if string(getStatusNode) == "not_registered" {
		LogInfo(statusGateway)
		LogInfo("register : node not registered")

		w.Write([]byte("not_registered"))
	} else {
		dumpToken = string(getStatusNode)
		LogInfo(statusGateway)
		infoToken := fmt.Sprintf("token : %s", string(getStatusNode))
		LogInfo(infoToken)

		w.Write([]byte(dumpToken))
	}
}

func verifyToken(token string) (status bool) {
	AESKeyGen()
	PassRead, err := CRTRead()
	if err != nil {
		log.Error(err)
	}

	v := url.Values{}
	v.Set("dev_id", cnf.DeviceCfg().DeviceID)
	v.Set("dev_pass", PassRead)
	v.Set("dev_mac", hwaddr)
	v.Set("dev_topic", cnf.MqttCfg().Topic)

	v.Set("node_pub_token", token)

	s := v.Encode()

	client := confKDM()

	reqVerifyTokenUrl := fmt.Sprintf("%s://%s:%d/node-token/", cnf.DeviceCfg().AuthProtocol, cnf.DeviceCfg().AuthUrl, cnf.DeviceCfg().AuthPort)
	reqVerifyToken, err := client.Post(reqVerifyTokenUrl, "application/x-www-form-urlencoded", strings.NewReader(s))
	if err != nil {
		log.Error(err)
		return
	}

	getStatusToken, err := ioutil.ReadAll(reqVerifyToken.Body)
	if err != nil {
		log.Error(err)
		return
	}
	defer reqVerifyToken.Body.Close()

	if string(getStatusToken) == "valid" {
		return true
	} else {
		return false
	}

	log.Info("response : ", reqVerifyToken.Status)
	log.Info("status : ", string(getStatusToken))

	return
}

func filterTopic(topic string) (filteredTopic string) {
	if topic == "#" {
		log.Info("Error Topic : The # topic is not allowed")
		os.Exit(1)
	} else {
		return topic
	}

	return topic
}
