package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"io"
	"io/ioutil"
	"os"
)

const (
	SecSslHttpPath       = "./.ssl/http/"
	SecSslHttpClientKey  = "./.ssl/http/client.key"
	SecSslHttpClientCert = "./.ssl/http/client.crt"
	SecSslHttpCert       = "./.ssl/http/ca.crt"

	SecSslHttpServerPath = "./.ssl-gateway/http/"
	SecSslHttpServerKey  = "./.ssl-gateway/http/server.key"
	SecSslHttpServerCert = "./.ssl-gateway/http/server.crt"
	SecSslHttpServerCA   = "./.ssl-gateway/http/ca.crt"

	TempSecSslHttpServerKey  = "./.ssl-gateway/http/temp-server.key"
	TempSecSslHttpServerCert = "./.ssl-gateway/http/temp-server.crt"

	SecSslMqttServerPath = "./.ssl-gateway/mqtt/"
	SecSslMqttServerKey  = "./.ssl-gateway/mqtt/server.key"
	SecSslMqttServerCert = "./.ssl-gateway/mqtt/server.crt"
	SecSslMqttServerCA   = "./.ssl-gateway/mqtt/ca.crt"
)

func InitServerSSl() {
	if _, err := os.Stat(SecSslHttpServerPath); err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(SecSslHttpServerPath, 755)
		}
	}

	if _, err := os.Stat(SecSslHttpServerKey); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().SrvKeyFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Gateway Key File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().SrvKeyFileDir, SecSslHttpServerKey)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslHttpServerKey)
			}
		}
	}

	if _, err := os.Stat(SecSslHttpServerCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().SrvCrtFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Gateway File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().SrvCrtFileDir, SecSslHttpServerCert)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslHttpServerCert)
			}
		}
	}

	if _, err := os.Stat(SecSslHttpServerCA); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().SrvCAFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Authority File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().SrvCAFileDir, SecSslHttpServerCA)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslHttpServerCA)
			}
		}
	}

	if _, err := os.Stat(SecSslMqttServerPath); err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(SecSslMqttServerPath, 755)
		}
	}

	if _, err := os.Stat(SecSslMqttServerKey); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.MqttCfg().SrvKeyFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Gateway Key File for MQTT-TLS Connection")
				}
			} else {
				err := os.Rename(cnf.MqttCfg().SrvKeyFileDir, SecSslMqttServerKey)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslMqttServerKey)
			}
		}
	}

	if _, err := os.Stat(SecSslMqttServerCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.MqttCfg().SrvCrtFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Gateway File for MQTT-TLS Connection")
				}
			} else {
				err := os.Rename(cnf.MqttCfg().SrvCrtFileDir, SecSslMqttServerCert)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslMqttServerCert)
			}
		}
	}

	if _, err := os.Stat(SecSslMqttServerCA); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.MqttCfg().SrvCAFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Authority File for MQTT-TLS Connection")
				}
			} else {
				err := os.Rename(cnf.MqttCfg().SrvCAFileDir, SecSslMqttServerCA)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslMqttServerCA)
			}
		}
	}
}

func UpdateServerHttpSSL() {
	if _, err := os.Stat(cnf.HttpCfg().SrvKeyFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Gateway Key File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().SrvKeyFileDir, SecSslHttpServerKey)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslHttpServerKey)
	}

	if _, err := os.Stat(cnf.HttpCfg().SrvCrtFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Server File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().SrvCrtFileDir, SecSslHttpServerCert)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslHttpServerCert)
	}

	if _, err := os.Stat(cnf.HttpCfg().SrvCAFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Authority File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().SrvCAFileDir, SecSslHttpServerCA)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslHttpServerCA)
	}
}

func UpdateServerMqttSSL() {
	if _, err := os.Stat(cnf.MqttCfg().SrvKeyFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Key File for MQTT-TLS Connection")
		}
	} else {
		err := os.Rename(cnf.MqttCfg().SrvKeyFileDir, SecSslMqttServerKey)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslMqttServerKey)
	}

	if _, err := os.Stat(cnf.MqttCfg().SrvCrtFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate File for MQTT-TLS Connection")
		}
	} else {
		err := os.Rename(cnf.MqttCfg().SrvCrtFileDir, SecSslMqttServerCert)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslMqttServerCert)
	}

	if _, err := os.Stat(cnf.MqttCfg().SrvCAFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Authority File for MQTT-TLS Connection")
		}
	} else {
		err := os.Rename(cnf.MqttCfg().SrvCAFileDir, SecSslMqttServerCA)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslMqttServerCA)
	}
}

func NewTLSConfig() *tls.Config {
	certpool := x509.NewCertPool()
	pemCerts, err := DencryptSSl(SecSslMqttServerCA)
	if err == nil {
		certpool.AppendCertsFromPEM(pemCerts)
	}

	cert, err := LoadX509KeyPairDec(SecSslMqttServerCert, SecSslMqttServerKey)
	if err != nil {
		log.Critical(err)
	}

	cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		log.Critical(err)
	}

	return &tls.Config{
		RootCAs:            certpool,
		ClientAuth:         tls.NoClientCert,
		ClientCAs:          nil,
		InsecureSkipVerify: true,
		Certificates:       []tls.Certificate{cert},
	}
}

func EncryptSSl(SSlPath string) {
	plaintext, err := ioutil.ReadFile(SSlPath)
	if err != nil {
		log.Critical(err)
	}

	AESKeyByte := []byte(AESKey)

	block, err := aes.NewCipher(AESKeyByte)
	if err != nil {
		log.Critical(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		log.Critical(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	os.Remove(SSlPath)
	os.Create(SSlPath)

	ioutil.WriteFile(SSlPath, ciphertext, 755)
}

func DencryptSSl(SSlPath string) ([]byte, error) {
	ciphertext, err := ioutil.ReadFile(SSlPath)
	if err != nil {
		log.Critical(err)
	}

	AESKeyByte := []byte(AESKey)

	block, err := aes.NewCipher(AESKeyByte)
	if err != nil {
		log.Critical(err)
	}

	if len(ciphertext) < aes.BlockSize {
		log.Critical("Text is too short")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)

	return ciphertext, err
}

func LoadX509KeyPairDec(certFile, keyFile string) (tls.Certificate, error) {
	certPEMBlock, err := DencryptSSl(certFile)
	if err != nil {
		return tls.Certificate{}, err
	}
	keyPEMBlock, err := DencryptSSl(keyFile)
	if err != nil {
		return tls.Certificate{}, err
	}
	return tls.X509KeyPair(certPEMBlock, keyPEMBlock)
}

func TempServerSSl() {
	SrvCrtFileDirDec, err := DencryptSSl(SecSslHttpServerCert)
	if err != nil {
		log.Fatal(err)
	}

	if _, err := os.Stat(TempSecSslHttpServerCert); err != nil {
		if os.IsNotExist(err) {
			os.Create(TempSecSslHttpServerCert)
			ioutil.WriteFile(TempSecSslHttpServerCert, []byte(SrvCrtFileDirDec), 755)
		}
	}

	SrvKeyFileDirDec, err := DencryptSSl(SecSslHttpServerKey)
	if err != nil {
		log.Fatal(err)
	}

	if _, err := os.Stat(TempSecSslHttpServerKey); err != nil {
		if os.IsNotExist(err) {
			os.Create(TempSecSslHttpServerKey)
			ioutil.WriteFile(TempSecSslHttpServerKey, []byte(SrvKeyFileDirDec), 755)
		}
	}
}
