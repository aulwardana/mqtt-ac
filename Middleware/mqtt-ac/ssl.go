package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"io"
	"io/ioutil"
	"os"
)

const (
	SecSslHttpPath       = "./.ssl/http/"
	SecSslHttpClientKey  = "./.ssl/http/client.key"
	SecSslHttpClientCert = "./.ssl/http/client.crt"
	SecSslHttpCert       = "./.ssl/http/ca.crt"
	SecSslMqttPath       = "./.ssl/mqtt/"
	SecSslMqttClientKey  = "./.ssl/mqtt/client.key"
	SecSslMqttClientCert = "./.ssl/mqtt/client.crt"
	SecSslMqttCert       = "./.ssl/mqtt/ca.crt"
)

func InitSSl() {
	if _, err := os.Stat(SecSslHttpPath); err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(SecSslHttpPath, 755)
		}
	}

	if _, err := os.Stat(SecSslHttpClientKey); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().KeyFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Client Key File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().KeyFileDir, SecSslHttpClientKey)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslHttpClientKey)
			}
		}
	}

	if _, err := os.Stat(SecSslHttpClientCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().CrtFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Client File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().CrtFileDir, SecSslHttpClientCert)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslHttpClientCert)
			}
		}
	}

	if _, err := os.Stat(SecSslHttpCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().CAFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Authority File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().CAFileDir, SecSslHttpCert)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslHttpCert)
			}
		}
	}

	if _, err := os.Stat(SecSslMqttPath); err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(SecSslMqttPath, 755)
		}
	}

	if _, err := os.Stat(SecSslMqttClientKey); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.MqttCfg().KeyFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Client Key File for MQTT-TLS Connection")
				}
			} else {
				err := os.Rename(cnf.MqttCfg().KeyFileDir, SecSslMqttClientKey)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslMqttClientKey)
			}
		}
	}

	if _, err := os.Stat(SecSslMqttClientCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.MqttCfg().CrtFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Client File for MQTT-TLS Connection")
				}
			} else {
				err := os.Rename(cnf.MqttCfg().CrtFileDir, SecSslMqttClientCert)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslMqttClientCert)
			}
		}
	}

	if _, err := os.Stat(SecSslMqttCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.MqttCfg().CAFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Authority File for MQTT-TLS Connection")
				}
			} else {
				err := os.Rename(cnf.MqttCfg().CAFileDir, SecSslMqttCert)
				if err != nil {
					log.Error(err)
				}

				EncryptSSl(SecSslMqttCert)
			}
		}
	}
}

func UpdateHttpSSL() {
	if _, err := os.Stat(cnf.HttpCfg().KeyFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Client Key File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().KeyFileDir, SecSslHttpClientKey)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslHttpClientKey)
	}

	if _, err := os.Stat(cnf.HttpCfg().CrtFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Client File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().CrtFileDir, SecSslHttpClientCert)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslHttpClientCert)
	}

	if _, err := os.Stat(cnf.HttpCfg().CAFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Authority File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().CAFileDir, SecSslHttpCert)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslHttpCert)
	}
}

func UpdateMqttSSL() {
	if _, err := os.Stat(cnf.MqttCfg().KeyFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Client Key File for MQTT-TLS Connection")
		}
	} else {
		err := os.Rename(cnf.MqttCfg().KeyFileDir, SecSslMqttClientKey)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslMqttClientKey)
	}

	if _, err := os.Stat(cnf.MqttCfg().CrtFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Client File for MQTT-TLS Connection")
		}
	} else {
		err := os.Rename(cnf.MqttCfg().CrtFileDir, SecSslMqttClientCert)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslMqttClientCert)
	}

	if _, err := os.Stat(cnf.MqttCfg().CAFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Authority File for MQTT-TLS Connection")
		}
	} else {
		err := os.Rename(cnf.MqttCfg().CAFileDir, SecSslMqttCert)
		if err != nil {
			log.Error(err)
		}

		EncryptSSl(SecSslMqttCert)
	}
}

func NewTLSConfig() *tls.Config {
	pemCerts, err := DencryptSSl(SecSslMqttCert)
	if err != nil {
		log.Critical(err)
	}
	certpool := x509.NewCertPool()
	certpool.AppendCertsFromPEM(pemCerts)

	cert, err := LoadX509KeyPairDec(SecSslMqttClientCert, SecSslMqttClientKey)
	if err != nil {
		log.Critical(err)
	}

	return &tls.Config{
		RootCAs:      certpool,
		Certificates: []tls.Certificate{cert},
	}
}

func EncryptSSl(SSlPath string) {
	plaintext, err := ioutil.ReadFile(SSlPath)
	if err != nil {
		log.Critical(err)
	}

	AESKeyByte := []byte(AESKey)

	block, err := aes.NewCipher(AESKeyByte)
	if err != nil {
		log.Critical(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		log.Critical(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	os.Remove(SSlPath)
	os.Create(SSlPath)

	ioutil.WriteFile(SSlPath, ciphertext, 755)
}

func DencryptSSl(SSlPath string) ([]byte, error) {
	ciphertext, err := ioutil.ReadFile(SSlPath)
	if err != nil {
		log.Critical(err)
	}

	AESKeyByte := []byte(AESKey)

	block, err := aes.NewCipher(AESKeyByte)
	if err != nil {
		log.Critical(err)
	}

	if len(ciphertext) < aes.BlockSize {
		log.Critical("Text is too short")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)

	return ciphertext, err
}

func LoadX509KeyPairDec(certFile, keyFile string) (tls.Certificate, error) {
	certPEMBlock, err := DencryptSSl(certFile)
	if err != nil {
		return tls.Certificate{}, err
	}
	keyPEMBlock, err := DencryptSSl(keyFile)
	if err != nil {
		return tls.Certificate{}, err
	}
	return tls.X509KeyPair(certPEMBlock, keyPEMBlock)
}
