package main

import (
	"bytes"
	"io/ioutil"
	"os"

	"github.com/spf13/viper"
)

const (
	MQTT_PROTOCOL   string = "mqtt.protocol"
	MQTT_ADDRESS    string = "mqtt.address"
	MQTT_PORT       string = "mqtt.port"
	MQTT_TOPIC      string = "mqtt.topic"
	MQTT_CLIENT_ID  string = "mqtt.clientid"
	MQTT_CAFILEDIR  string = "mqtt.cafiledir"
	MQTT_CRTFILEDIR string = "mqtt.crtfiledir"
	MQTT_KEYFILEDIR string = "mqtt.keyfiledir"
	HTTP_PROTOCOL   string = "http.protocol"
	HTTP_ADDRESS    string = "http.address"
	HTTP_PORT       string = "http.port"
	HTTP_CAFILEDIR  string = "http.cafiledir"
	HTTP_CRTFILEDIR string = "http.crtfiledir"
	HTTP_KEYFILEDIR string = "http.keyfiledir"
	DEVICE_ID       string = "device.id"
)

type MqttConfig struct {
	Protocol   string
	Address    string
	Port       int
	Topic      string
	ClientID   string
	CAFileDir  string
	CrtFileDir string
	KeyFileDir string
}

type HttpConfig struct {
	Protocol   string
	Address    string
	Port       int
	CAFileDir  string
	CrtFileDir string
	KeyFileDir string
}

type DeviceConfig struct {
	DeviceID string
}

type Config struct {
	*viper.Viper
}

func setDefaults(v *viper.Viper) {
	v.SetDefault(MQTT_PROTOCOL, "tcp")
	v.SetDefault(MQTT_ADDRESS, "localhost")
	v.SetDefault(MQTT_PORT, 1883)
	v.SetDefault(MQTT_TOPIC, "cofidential")
	v.SetDefault(MQTT_CLIENT_ID, "mqtt-ac")
	v.SetDefault(MQTT_CAFILEDIR, "ssl-cert/mqtt/CAfile.pem")
	v.SetDefault(MQTT_CRTFILEDIR, "ssl-cert/mqtt/client-crt.pem")
	v.SetDefault(MQTT_KEYFILEDIR, "ssl-cert/mqtt/client-key.pem")
	v.SetDefault(HTTP_PROTOCOL, "http")
	v.SetDefault(HTTP_ADDRESS, "localhost")
	v.SetDefault(HTTP_PORT, 8443)
	v.SetDefault(HTTP_CAFILEDIR, "ssl-cert/http/CAfile.pem")
	v.SetDefault(HTTP_CRTFILEDIR, "ssl-cert/http/client-crt.pem")
	v.SetDefault(HTTP_KEYFILEDIR, "ssl-cert/http/client-key.pem")
	v.SetDefault(DEVICE_ID, " ")
}

func loadConfPath(v *viper.Viper, path string) error {
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	f, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	return v.ReadConfig(bytes.NewBuffer(f))
}

func (c *Config) MqttCfg() *MqttConfig {
	return &MqttConfig{
		Protocol:   c.GetString(MQTT_PROTOCOL),
		Address:    c.GetString(MQTT_ADDRESS),
		Port:       c.GetInt(MQTT_PORT),
		Topic:      c.GetString(MQTT_TOPIC),
		ClientID:   c.GetString(MQTT_CLIENT_ID),
		CAFileDir:  c.GetString(MQTT_CAFILEDIR),
		CrtFileDir: c.GetString(MQTT_CRTFILEDIR),
		KeyFileDir: c.GetString(MQTT_KEYFILEDIR),
	}
}

func (c *Config) HttpCfg() *HttpConfig {
	return &HttpConfig{
		Protocol:   c.GetString(HTTP_PROTOCOL),
		Address:    c.GetString(HTTP_ADDRESS),
		Port:       c.GetInt(HTTP_PORT),
		CAFileDir:  c.GetString(HTTP_CAFILEDIR),
		CrtFileDir: c.GetString(HTTP_CRTFILEDIR),
		KeyFileDir: c.GetString(HTTP_KEYFILEDIR),
	}
}

func (c *Config) DeviceCfg() *DeviceConfig {
	return &DeviceConfig{
		DeviceID: c.GetString(DEVICE_ID),
	}
}

func NewCfg(path ...string) (*Config, error) {
	v := viper.New()
	v.SetConfigType("yaml")
	setDefaults(v)

	if len(path) > 0 {
		if err := loadConfPath(v, path[0]); err != nil {
			log.Warning("Failed load from file. Mqtt-ac using default configuration")
		}
	}

	return &Config{v}, nil
}
