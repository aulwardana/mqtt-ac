package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
	"io/ioutil"
	"os"
)

const (
	crtFolderPath  = "./.crt/"
	credentialPath = "./.crt/identity.aul"
	AESKeyPath     = "./.crt/aes.aul"
)

var (
	AESKey string
)

func AESInit() {
	if _, err := os.Stat(crtFolderPath); err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(crtFolderPath, 755)
		}
	}

	if _, err := os.Stat(credentialPath); err != nil {
		if os.IsNotExist(err) {
			os.Create(credentialPath)
		}
	}

	if _, err := os.Stat(AESKeyPath); err != nil {
		if os.IsNotExist(err) {
			os.Create(AESKeyPath)
			g := Generator{}
			g.NewToken()
			ioutil.WriteFile(AESKeyPath, []byte(g.GetToken(16)), 755)
		}
	}

	AESKeyGen()
}

func CRTCheck() bool {
	if CRTFile() == false && regKDM() == false {
		LogInfo("Registered")
		return true
	} else if CRTFile() == false && regKDM() == true {
		log.Info("Credential Changed")
		return false
	} else if CRTFile() == true && regKDM() == true {
		LogInfo("Registered")
		return true
	} else if CRTFile() == true && regKDM() == false {
		log.Info("Credential Fake")
		return false
	}

	return false
}

func CRTWrite(credentialStr string) {
	ciphertext := EncryptSTR(credentialStr, AESKey)
	ioutil.WriteFile(credentialPath, []byte(ciphertext), 755)
}

func CRTRead() (string, error) {
	data, err := ioutil.ReadFile(credentialPath)
	credentialStr := DecryptSTR(string(data), AESKey)
	return credentialStr, err
}

func CRTFile() bool {
	data, err := ioutil.ReadFile(credentialPath)
	if err != nil {
		log.Error(err)
		return false
	}

	if string(data) != "" {
		return true
	} else {
		return false
	}

	return false
}

func AESKeyGen() {
	data, err := ioutil.ReadFile(AESKeyPath)
	if err != nil {
		log.Critical(err)
	}
	AESKey = string(data)
}

func EncryptSTR(plainstring, keystring string) string {
	plaintext := []byte(plainstring)
	AESKey := []byte(keystring)

	block, err := aes.NewCipher(AESKey)
	if err != nil {
		log.Critical(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		log.Critical(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	return string(ciphertext)
}

func DecryptSTR(cipherstring string, keystring string) string {
	ciphertext := []byte(cipherstring)
	AESKey := []byte(keystring)

	block, err := aes.NewCipher(AESKey)
	if err != nil {
		log.Critical(err)
	}

	if len(ciphertext) < aes.BlockSize {
		log.Error("Text is too short")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)

	return string(ciphertext)
}
