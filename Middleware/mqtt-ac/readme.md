# Middleware Publisher/Subscriber App


This application is security client tool for publisher or subscriber. This application use to help another service that use publish/subscribe from MQTT protocol communication model to secure with authentication and authorization process and validate the integrity and confidentiality from the sensing data.


## VPS Preparation for Subscriber
1. Use VM or VPS with [Cent OS](https://www.centos.org/download/ "Cent OS") installed inside.
2. Install Mosquitto MQTT. 
3. Open port 8883 in VM or VPS.


  ``` bash
  sudo firewall-cmd --permanent --add-port=8883/tcp
  sudo firewall-cmd --reload
  ```


## Raspbery Pi 3 Preparation for Publisher
This research use Raspbery Pi 3 Model B as device gateway. The publisher middleware app will be deploy in raspbery pi device. The requirement from the raspbery is :
1. The raspbery pi must use [Raspbian OS](https://www.raspberrypi.org/downloads/raspbian/ "RASPBIAN STRETCH WITH DESKTOP") for the operating system.
2. After that the SSH option from raspbian os must active to remote the raspbery pi.


## Generate SSL Certificate
1. Log in to authentication server VPS via SSH.
2. Generate SSL Certificate using our script in `mqtt-ac/Tools/` folder.
3. For development in localhost you can use SSL Certificate in `master/Tools/ssl-cert` folder.


## Configure Secure Broker in VPS
First to configure the secure broker please follow [this link](https://mcuoneclipse.com/2017/04/14/enable-secure-communication-with-tls-and-the-mosquitto-broker/ "MQTT Broker SSL/TLS").


## Config File for Publisher App in Device Gateway
Please configure the config file `config-sample.yaml` in `mqtt-ac/Middleware/mqtt-ac` folder, sample configuration is like this :


  ``` bash
  mqtt: 
    protocol: "ssl"
    address: "35.202.152.183"
    port: 8883
    topic: "confidential"
    clientid: "mqtt-ac"
    cafiledir: "ssl-cert/mqtt/ca1.crt"
    crtfiledir: "ssl-cert/mqtt/client1.crt"
    keyfiledir: "ssl-cert/mqtt/client1.key"
  http: 
    protocol: "https"
    address: "104.154.195.155"
    port: 443
    cafiledir: "ssl-cert/http/server2.crt"
    crtfiledir: "ssl-cert/http/client2.crt"
    keyfiledir: "ssl-cert/http/client2.key"
  device:
    id: "device1"
  ```


After configuration finish, please rename this configuration with `config-ac.yaml` and add this configuration in same folder with publisher app.


## Config File for Subscriber App in VPS
Please configure the config file `config-sample.yaml` in `mqtt-ac/Middleware/mqtt-ac` folder, sample configuration is like this :


  ``` bash
  mqtt: 
    protocol: "ssl"
    address: "35.202.152.183"
    port: 8883
    topic: "confidential"
    clientid: "mqtt-ac"
    cafiledir: "ssl-cert/mqtt/ca1.crt"
    crtfiledir: "ssl-cert/mqtt/client1.crt"
    keyfiledir: "ssl-cert/mqtt/client1.key"
  http: 
    protocol: "https"
    address: "104.154.195.155"
    port: 443
    cafiledir: "ssl-cert/http/server2.crt"
    crtfiledir: "ssl-cert/http/client2.crt"
    keyfiledir: "ssl-cert/http/client2.key"
  device:
    id: ""
  ```


After configuration finish, please rename this configuration with `config-ac.yaml` and add this configuration in same folder with subscriber app.


## Build Publisher App in Device Gateway
For publisher app, you must build with special command like this :


  ``` bash
  GOARM=7 GOOS=linux GOARCH=arm go build main.go auth.go config.go socket.go ssl.go sec-crt.go token.go
  ```


The build command is special because the publisher app will deploy in raspbery pi that use ARM processor to running the app. We must build the app with defined specific operating system and processor architecture.


## Build Subscriber App in VPS
For subscriber app, you can build the application with normal mode using LiteIDE or `go build` command.


## Middleware App Options
You can use help command to show all options from this app, the command is : 


  ``` bash
  $ ./main --help
  ```


it will be show like this :


  ``` bash
  Options:
  [-help]                      Display help
  [-a pub|sub]                 Action pub (publish) or sub (subscribe)
  [-m <message>]               Payload to send
  [-u http|mqtt]               Action http (update http ssl) or mqtt (update mqtt ssl)
  [-c <path/config.yaml>]      Use custom configuration file (config.yaml)
  ```


The SSL certificate is updateable, you can copy the update file from SSL certificate to SSL folder in configuration file and run update command.