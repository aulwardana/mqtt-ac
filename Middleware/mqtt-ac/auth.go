package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
)

var (
	dumpToken string
)

func confKDM() (session *http.Client) {
	caCert, err := DencryptSSl(SecSslHttpCert)
	if err != nil {
		log.Critical(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	cert, err := LoadX509KeyPairDec(SecSslHttpClientCert, SecSslHttpClientKey)
	if err != nil {
		log.Critical(err)
	}

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      caCertPool,
				Certificates: []tls.Certificate{cert},
			},
		},
	}

	return client
}

func conKDM() {
	client := confKDM()

	urlHttp := fmt.Sprintf("%s://%s:%d", cnf.HttpCfg().Protocol, cnf.HttpCfg().Address, cnf.HttpCfg().Port)
	resp, err := client.Get(urlHttp)
	if err != nil {
		log.Error(err)
		return
	}

	htmlData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error(err)
		return
	}
	defer resp.Body.Close()
	conKDMResp := fmt.Sprintf("response : %s", resp.Status)
	conKDMMsg := fmt.Sprint("message : %s", string(htmlData))

	LogInfo(conKDMResp)
	LogInfo(conKDMMsg)
}

func checkKDM() bool {
	v := url.Values{}
	v.Set("dev_id", cnf.DeviceCfg().DeviceID)
	v.Set("dev_mac", hwaddr)
	v.Set("dev_topic", cnf.MqttCfg().Topic)

	s := v.Encode()

	client := confKDM()

	reqAuthUrl := fmt.Sprintf("%s://%s:%d/check/", cnf.HttpCfg().Protocol, cnf.HttpCfg().Address, cnf.HttpCfg().Port)
	reqAuth, err := client.Post(reqAuthUrl, "application/x-www-form-urlencoded", strings.NewReader(s))
	if err != nil {
		log.Error(err)

		return false
	}

	getPasswd, err := ioutil.ReadAll(reqAuth.Body)
	if err != nil {
		log.Error(err)

		return false
	}
	defer reqAuth.Body.Close()

	regGatewayStatus := fmt.Sprintf("response : %s", reqAuth.Status)

	if string(getPasswd) == "registered" {
		LogInfo(regGatewayStatus)
		LogInfo("register : device registered")

		return true
	} else {
		LogInfo(regGatewayStatus)
		LogInfo("register : device not registered")

		return false
	}
}

func authKDM() (status bool) {
	AESKeyGen()
	PassRead, err := CRTRead()
	if err != nil {
		log.Error(err)
	}

	v := url.Values{}
	v.Set("dev_id", cnf.DeviceCfg().DeviceID)
	v.Set("dev_pass", PassRead)
	v.Set("dev_mac", hwaddr)
	v.Set("dev_topic", cnf.MqttCfg().Topic)

	s := v.Encode()

	client := confKDM()

	reqAuthUrl := fmt.Sprintf("%s://%s:%d/auth/", cnf.HttpCfg().Protocol, cnf.HttpCfg().Address, cnf.HttpCfg().Port)
	reqAuth, err := client.Post(reqAuthUrl, "application/x-www-form-urlencoded", strings.NewReader(s))
	if err != nil {
		log.Error(err)
		return
	}

	getToken, err := ioutil.ReadAll(reqAuth.Body)
	if err != nil {
		log.Error(err)
		return
	}
	defer reqAuth.Body.Close()

	authGatewayStatus := fmt.Sprintf("response : %s", reqAuth.Status)

	if string(getToken) == "invalid_auth" {
		LogInfo(authGatewayStatus)
		LogInfo("token : invalid auth")

		return false
	} else if string(getToken) == "deny" {
		LogInfo(authGatewayStatus)
		LogInfo("token : access denied")

		return false
	} else {
		dumpToken = string(getToken)
		LogInfo(authGatewayStatus)
		authTokenInfo := fmt.Sprintf("token : %s", string(getToken))
		LogInfo(authTokenInfo)

		return true
	}

	return
}

func regKDM() bool {
	v := url.Values{}
	v.Set("dev_id", cnf.DeviceCfg().DeviceID)
	v.Set("dev_mac", hwaddr)
	v.Set("dev_topic", cnf.MqttCfg().Topic)

	s := v.Encode()

	client := confKDM()

	reqAuthUrl := fmt.Sprintf("%s://%s:%d/reg/", cnf.HttpCfg().Protocol, cnf.HttpCfg().Address, cnf.HttpCfg().Port)
	reqAuth, err := client.Post(reqAuthUrl, "application/x-www-form-urlencoded", strings.NewReader(s))
	if err != nil {
		log.Error(err)

		return false
	}

	getPasswd, err := ioutil.ReadAll(reqAuth.Body)
	if err != nil {
		log.Error(err)

		return false
	}
	defer reqAuth.Body.Close()

	regGatewayStatusReg := fmt.Sprintf("response : %s", reqAuth.Status)

	if string(getPasswd) == "is_registered" {
		LogInfo(regGatewayStatusReg)
		LogInfo("register : device already registered")

		return true
	} else {
		AESKeyGen()
		CRTWrite(string(getPasswd))

		LogInfo(regGatewayStatusReg)
		LogInfo("register : registration success")

		return false
	}
}

func verifyKDM(token string) (status bool) {
	v := url.Values{}
	v.Set("pub_token", token)

	s := v.Encode()

	client := confKDM()

	reqVerifyTokenUrl := fmt.Sprintf("%s://%s:%d/validate/", cnf.HttpCfg().Protocol, cnf.HttpCfg().Address, cnf.HttpCfg().Port)
	reqVerifyToken, err := client.Post(reqVerifyTokenUrl, "application/x-www-form-urlencoded", strings.NewReader(s))
	if err != nil {
		log.Error(err)
		return
	}

	getStatusToken, err := ioutil.ReadAll(reqVerifyToken.Body)
	if err != nil {
		log.Error(err)
		return
	}
	defer reqVerifyToken.Body.Close()

	if string(getStatusToken) == "valid" {
		return true
	} else {
		return false
	}

	log.Info("response : ", reqVerifyToken.Status)
	log.Info("status : ", string(getStatusToken))

	return
}

func filterTopic(topic string) (filteredTopic string) {
	if topic == "#" {
		log.Info("Error Topic : The # topic is not allowed")
		os.Exit(1)
	} else {
		return topic
	}

	return topic
}
