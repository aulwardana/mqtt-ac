package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"runtime"
	"sync"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/op/go-logging"
)

var (
	cnf        *Config
	configPath string
)

type SecurePayload struct {
	Token   string
	Payload string
}

var log = logging.MustGetLogger("secure-log-style")

var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:05x}%{color:reset} %{message}`,
)

var f MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	var m SecurePayload

	if err := json.Unmarshal(msg.Payload(), &m); err != nil {
		panic(err)
	}

	if verifyKDM(m.Token) == true {
		LogPayload(msg.Topic(), m.Payload)
	} else {
		log.Notice("Token Invalid")
	}
}

/*
Options:
 [-help]                      Display help
 [-v]                         Display detail logging (verbose mode)
 [-a pub|sub]                 Action pub (publish) or sub (subscribe)
 [-m <message>]               Payload to send
 [-u http|mqtt]               Action http (update http ssl) or mqtt (update mqtt ssl)
 [-c <path/config.yaml>]      Use custom configuration file (config.yaml)
*/

func initConfig() error {
	flag.StringVar(&configPath, "c", "config-ac.yaml", "Configuration File")

	c, err := NewCfg(configPath)
	if err != nil {
		return err
	}
	cnf = c

	return err
}

func main() {
	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend2 := logging.NewLogBackend(os.Stderr, "", 0)

	backend2Formatter := logging.NewBackendFormatter(backend2, format)
	backend1Leveled := logging.AddModuleLevel(backend1)
	backend1Leveled.SetLevel(logging.ERROR, "")

	logging.SetBackend(backend1Leveled, backend2Formatter)

	payload := flag.String("m", "", "The message text to publish (default empty)")
	action := flag.String("a", "", "Action pub (publish) or sub (subscribe)")
	update := flag.String("u", "", "Action http (update http ssl) or mqtt (update mqtt ssl)")
	verbosLogeMode := flag.Bool("v", false, "Used to detail logging (verbose mode)")

	if err := initConfig(); err != nil {
		log.Error(err)
	}

	flag.Parse()

	LogStatus(*verbosLogeMode)

	AESInit()

	InitSSl()

	tlsconfig := NewTLSConfig()

	hostMQTT := fmt.Sprintf("%s://%s:%d", cnf.MqttCfg().Protocol, cnf.MqttCfg().Address, cnf.MqttCfg().Port)

	SocketInfo()

	conKDM()

	if *action == "pub" {
		if checkKDM() == true {
			if CRTCheck() == true {
				if authKDM() == true {
					opts := MQTT.NewClientOptions().AddBroker(hostMQTT).SetTLSConfig(tlsconfig)

					c := MQTT.NewClient(opts)
					if token := c.Connect(); token.Wait() && token.Error() != nil {
						log.Critical(token.Error())
					}

					authPayload := &SecurePayload{
						Token:   dumpToken,
						Payload: *payload,
					}

					p, err := json.Marshal(authPayload)
					if err != nil {
						log.Error(err)
						return
					}

					token := c.Publish(cnf.MqttCfg().Topic, 0, false, string(p))
					token.Wait()

					pubMqttHostInfo := fmt.Sprintf("Publish to: %s", hostMQTT)
					pubMqttTopicInfo := fmt.Sprintf("Topic: %s", cnf.MqttCfg().Topic)
					pubMqttMsgInfo := fmt.Sprintf("Published Message: %s", *payload)

					LogDebug(pubMqttHostInfo)
					LogDebug(pubMqttTopicInfo)
					log.Debug(pubMqttMsgInfo)

					c.Disconnect(250)
				} else {
					log.Critical("Token Invalid and Data Not Send !")
				}
			} else {
				os.Exit(0)
			}
		} else {
			log.Info("Please contact Administrator to Register Device")
			os.Exit(0)
		}
	} else if *action == "sub" {
		runtime.GOMAXPROCS(1)

		var wg sync.WaitGroup
		wg.Add(2)

		opts := MQTT.NewClientOptions().AddBroker(hostMQTT)
		opts.SetClientID(cnf.MqttCfg().ClientID).SetTLSConfig(tlsconfig)
		opts.SetDefaultPublishHandler(f)

		c := MQTT.NewClient(opts)
		if token := c.Connect(); token.Wait() && token.Error() != nil {
			log.Error(token.Error())
		}

		go func() {
			defer wg.Done()

			mqttPortDebug := fmt.Sprintf("Subscribe Secure-MQTT Start on: %s", hostMQTT)
			mqttTopicDebug := fmt.Sprintf("Topic: %s", cnf.MqttCfg().Topic)
			LogDebug(mqttPortDebug)
			LogDebug(mqttTopicDebug)

			if token := c.Subscribe(filterTopic(cnf.MqttCfg().Topic), 0, nil); token.Wait() && token.Error() != nil {
				log.Error(token.Error())
				os.Exit(1)
			}
		}()

		wg.Wait()
	} else {
		log.Info("Invalid setting for -action, must be pub or sub")
	}

	if *update == "http" {
		UpdateHttpSSL()
	} else if *update == "mqtt" {
		UpdateMqttSSL()
	} else {
		LogInfo("Invalid setting for -update ssl, must be http or mqtt")
	}
}
