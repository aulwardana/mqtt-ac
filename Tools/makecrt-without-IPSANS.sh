#!/bin/bash
# call this script with an Sountry ID, Province, City, Departement, Sub-Departement, Url Server, Email (valid or not).
# like:
# ./makecrt.sh ID West-Java Bandung ITB STEI localhost auliawardan@gmail.com
# ./makecrt.sh ID East-Java Malang UMM FT localhost auldesain@gmail.com
echo "create CA"
openssl req -newkey rsa:2048 -nodes -days 3650 -x509 -keyout ca.key -out ca.crt -subj "/CN=$6/emailAddress=$7"
echo "create server key"
openssl req -newkey rsa:2048 -nodes -keyout server.key -out server.csr -subj "/C=$1/ST=$2/L=$3/O=$4/OU=$5/CN=$6/emailAddress=$7"
echo "create server cert"
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt
echo "create client key and cert"
openssl req -x509 -nodes -newkey rsa:2048 -keyout client.key -out client.crt -days 3650 -subj "/C=$1/ST=$2/L=$3/O=$4/OU=$5/CN=$6/emailAddress=$7"
