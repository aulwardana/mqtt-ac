#!/bin/bash
# call this script with an Sountry ID, Province, City, Departement, Sub-Departement, Url Server, Email (valid or not).
# like:
# ./makecrt.sh ID West-Java Bandung ITB STEI 192.168.1.1 auliawardan@gmail.com
echo "create CA"
openssl req -newkey rsa:2048 -nodes -days 3650 -x509 -keyout ca2.key -out ca2.crt -subj "/CN=$6/emailAddress=$7"
echo "create server key"
openssl req -newkey rsa:2048 -nodes -keyout server2.key -out server2.csr -subj "/C=$1/ST=$2/L=$3/O=$4/OU=$5/CN=$6/emailAddress=$7"
echo "create server cert"
openssl x509 -req -days 365 -sha256 -in server2.csr -CA ca2.crt -CAkey ca2.key -CAcreateserial -out server2.crt -extfile <(echo subjectAltName = IP:$6)
echo "create client key and cert"
openssl req -x509 -nodes -newkey rsa:2048 -keyout client2.key -out client2.crt -days 3650 -subj "/C=$1/ST=$2/L=$3/O=$4/OU=$5/CN=$6/emailAddress=$7"
