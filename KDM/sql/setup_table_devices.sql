﻿CREATE TABLE devices
(
uid serial NOT NULL,
devid character varying(255) NOT NULL,
devpass character varying(500) NOT NULL,
devmac character varying(255) NOT NULL,
devtopic character varying(255) NOT NULL,
regtime date,
CONSTRAINT devices_pkey PRIMARY KEY (uid)
)
WITH (OIDS=FALSE);