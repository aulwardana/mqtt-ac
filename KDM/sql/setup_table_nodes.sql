﻿CREATE TABLE nodes
(
uid serial NOT NULL,
devid character varying(255) NOT NULL,
devpass character varying(255) NOT NULL,
devmac character varying(255) NOT NULL,
devtopic character varying(255) NOT NULL,
devgateway character varying(255) NOT NULL,
regtime date,
CONSTRAINT nodes_pkey PRIMARY KEY (uid)
)
WITH (OIDS=FALSE);