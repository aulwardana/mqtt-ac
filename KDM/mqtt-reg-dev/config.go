package main

import (
	"bytes"
	"io/ioutil"
	"os"

	"github.com/spf13/viper"
)

const (
	PG_HOST     string = "pg.host"
	PG_PORT     string = "pg.port"
	PG_USER     string = "pg.user"
	PG_PASSWORD string = "pg.password"
	PG_DBNAME   string = "pg.dbname"
	PG_SSLMODE  string = "pg.sslmode"
)

type PgConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	DBName   string
	SSLMode  string
}

type Config struct {
	*viper.Viper
}

func setDefaults(v *viper.Viper) {
	v.SetDefault(PG_HOST, "localhost")
	v.SetDefault(PG_PORT, 5432)
	v.SetDefault(PG_USER, "postgres")
	v.SetDefault(PG_PASSWORD, "postgres")
	v.SetDefault(PG_DBNAME, "postgres")
	v.SetDefault(PG_SSLMODE, "disable")
}

func loadConfPath(v *viper.Viper, path string) error {
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	f, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	return v.ReadConfig(bytes.NewBuffer(f))
}

func (c *Config) PostgresCfg() *PgConfig {
	return &PgConfig{
		Host:     c.GetString(PG_HOST),
		Port:     c.GetInt(PG_PORT),
		User:     c.GetString(PG_USER),
		Password: c.GetString(PG_PASSWORD),
		DBName:   c.GetString(PG_DBNAME),
		SSLMode:  c.GetString(PG_SSLMODE),
	}
}

func NewCfg(path ...string) (*Config, error) {
	v := viper.New()
	v.SetConfigType("yaml")
	setDefaults(v)

	if len(path) > 0 {
		if err := loadConfPath(v, path[0]); err != nil {
			log.Warning("Failed load from file. Mqtt-reg-dev using default configuration")
		}
	}

	return &Config{v}, nil
}
