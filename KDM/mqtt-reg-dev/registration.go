package main

import (
	"database/sql"
	"fmt"
	"sync"
	"time"

	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

type DeviceProfile struct {
	Uid      int
	DevId    string
	DevMac   string
	DevPass  string
	DevTopic string
	RegTime  time.Time
}

type DeviceNodeProfile struct {
	Uid            int
	DevNodeId      string
	DevNodeMac     string
	DevNodePass    string
	DevNodeTopic   string
	DevNodeGateway string
	RegTime        time.Time
}

var db *sql.DB
var once sync.Once

func GetConnection() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s", cnf.PostgresCfg().Host, cnf.PostgresCfg().Port, cnf.PostgresCfg().User, cnf.PostgresCfg().Password, cnf.PostgresCfg().DBName, cnf.PostgresCfg().SSLMode)

	once.Do(func() {
		var err error
		if db, err = sql.Open("postgres", psqlInfo); err != nil {
			log.Panic(err)
		}
		db.SetMaxOpenConns(20)
		db.SetMaxIdleConns(0)
		db.SetConnMaxLifetime(time.Nanosecond)
	})
	return db
}

func CheckDevice(devId string, devMac string, devTopic string) (status bool) {

	tx, err := db.Begin()
	defer tx.Rollback()
	rows, err := db.Query("SELECT * FROM public.devices WHERE devmac = $1", devMac)
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer rows.Close()

	for rows.Next() {
		c := DeviceProfile{}
		err := rows.Scan(&c.Uid, &c.DevId, &c.DevPass, &c.DevMac, &c.DevTopic, &c.RegTime)
		if err != nil {
			log.Fatal(err)
		}

		if c.DevMac == devMac && c.DevId == devId && c.DevTopic == devTopic {
			return true
		} else {
			return false
		}
	}

	return false
}

func CheckNodeDevice(devNodeId string, devNodeMac string, devNodeTopic string, devGateway string) (status bool) {

	tx, err := db.Begin()
	defer tx.Rollback()
	rows, err := db.Query("SELECT * FROM public.nodes WHERE devmac = $1", devNodeMac)
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer rows.Close()

	for rows.Next() {
		c := DeviceNodeProfile{}
		err := rows.Scan(&c.Uid, &c.DevNodeId, &c.DevNodePass, &c.DevNodeMac, &c.DevNodeTopic, &c.RegTime)
		if err != nil {
			log.Fatal(err)
		}

		if c.DevNodeMac == devNodeMac && c.DevNodeId == devNodeId && c.DevNodeTopic == devNodeTopic && c.DevNodeGateway == devGateway {
			return true
		} else {
			return false
		}
	}

	return false
}

func RegisterDevice(devId string, devMac string, devTopic string) {
	if CheckDevice(devId, devMac, devTopic) == true {
		log.Info("Device Already Registered")
	} else {
		regTime := time.Now()
		devPassword := ""

		var lastInsertId int
		db.QueryRow("INSERT INTO public.devices(devid, devpass, devmac, devtopic, regtime) VALUES($1,$2,$3,$4,$5) returning uid;", devId, devPassword, devMac, devTopic, regTime).Scan(&lastInsertId)
		if lastInsertId == 0 {
			log.Info("Registration Failed")
		} else {
			log.Info("Registration Success")
		}
	}
}

func RegisterNodeDevice(devNodeId string, devNodeMac string, devNodeTopic string, devGateway string) {
	if CheckNodeDevice(devNodeId, devNodeMac, devNodeTopic, devGateway) == true {
		log.Info("Device Node Already Registered")
	} else {
		regTime := time.Now()
		g := Generator{}
		g.NewToken()
		devNodePass := g.GetToken(8)
		hashpaswd := hashAndSalt([]byte(devNodePass))

		var lastInsertId int
		db.QueryRow("INSERT INTO public.nodes(devid, devpass, devmac, devtopic, devgateway, regtime) VALUES($1,$2,$3,$4,$5,$6) returning uid;", devNodeId, hashpaswd, devNodeMac, devNodeTopic, devGateway, regTime).Scan(&lastInsertId)
		if lastInsertId == 0 {
			log.Info("Registration Failed")
		} else {
			log.Info("Registration Success")
			log.Info("Password for : ", devNodeId, ", is : ", devNodePass)
		}
	}
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Error(err)
	}

	return string(hash)
}
