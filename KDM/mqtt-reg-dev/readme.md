# Device Node and Gateway Registration App


This application use to register device node and device gateway that will connect to authentication server.


## VPS Preparation
1. Use VM or VPS with [Cent OS](https://www.centos.org/download/ "Cent OS") installed inside.
2. Install PostgreSQL. You can use pgAdmin to remote the PostgreSQL for development purpose. Download via [this link](https://www.pgadmin.org/ "pgAdmin").
3. Install Redis. You can use Redis Desktop Manager to remote the Redis for development purpose. Download via [this link](https://redisdesktop.com/ "RDM").


## Config File
Please configure the config file `config-sample.yaml` in `mqtt-ac/KDM/mqtt-reg-dev` folder, sample configuration is like this :


  ``` bash
  pg: 
    host: "localhost"
    port: 5432
    user: "postgres"
    password: "postgres"
    dbname: "postgres"
    sslmode: "disable"
  ```


After configuration finish, please rename this configuration with `config.yaml` and add this configuration in same folder with KDM app.

