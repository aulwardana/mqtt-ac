package main

import (
	"flag"
	"os"

	"github.com/op/go-logging"
)

var (
	cnf        *Config
	configPath string
)

var log = logging.MustGetLogger("secure-log-style")

var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:05x}%{color:reset} %{message}`,
)

/*
Options:
 [-help]                      Display help
 [-a gateway|node]            Action gateway (register device gateway) or node (register device node)
 [-d <Device ID>]             Device ID
 [-m <Mac Address>]           Mac Address
 [-t <Topic>]                 Topic
 [-g <Gateway>]               Device ID from Gateway
 [-c <path/config.yaml>]      Use custom configuration file (config.yaml)
*/

func initConfig() error {
	flag.StringVar(&configPath, "c", "config.yaml", "Configuration File")
	flag.Parse()

	c, err := NewCfg(configPath)
	if err != nil {
		return err
	}
	cnf = c

	return err
}

func main() {
	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend2 := logging.NewLogBackend(os.Stderr, "", 0)

	backend2Formatter := logging.NewBackendFormatter(backend2, format)
	backend1Leveled := logging.AddModuleLevel(backend1)
	backend1Leveled.SetLevel(logging.ERROR, "")

	logging.SetBackend(backend1Leveled, backend2Formatter)

	Actions := flag.String("a", "", "Action gateway (register device gateway) or node (register device node)")
	DeviceID := flag.String("d", "", "Device ID (can not be empty)")
	MacAddress := flag.String("m", "", "Mac Address (can not be empty)")
	Topic := flag.String("t", "", "Topic (can not be empty)")
	Gateway := flag.String("g", "", "Device ID from Gateway (can not be empty)")

	err := initConfig()
	if err != nil {
		log.Fatal(err)
	}

	db = GetConnection()

	if *Actions == "gateway" {
		if *DeviceID != "" && *MacAddress != "" && *Topic != "" {
			RegisterDevice(string(*DeviceID), string(*MacAddress), string(*Topic))
		} else {
			log.Info("Device ID, Mac Address, and Topic can not be empty")
		}
	} else if *Actions == "node" {
		if *DeviceID != "" && *MacAddress != "" && *Topic != "" && *Gateway != "" {
			RegisterNodeDevice(string(*DeviceID), string(*MacAddress), string(*Topic), string(*Gateway))
		} else {
			log.Info("Device ID, Mac Address, Topic, and Gateway Device ID can not be empty")
		}
	} else {
		log.Info("Invalid setting for -action, must be gateway or node")
	}
}
