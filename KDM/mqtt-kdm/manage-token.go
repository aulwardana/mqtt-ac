package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/garyburd/redigo/redis"
)

var (
	Pool *redis.Pool
)

type TokenProfile struct {
	Token       string    `json:"token"`
	DeviceId    string    `json:"dev_id"`
	DeviceMac   string    `json:"dev_mac"`
	DeviceTopic string    `json:"dev_topic"`
	Timestamp   time.Time `json:"time"`
}

type TokenNodeProfile struct {
	Token         string    `json:"token"`
	DeviceId      string    `json:"dev_id"`
	DeviceMac     string    `json:"dev_mac"`
	DeviceTopic   string    `json:"dev_topic"`
	DeviceGateway string    `json:"dev_gateway"`
	Timestamp     time.Time `json:"time"`
}

func RdsInit() {
	Pool = ConnRds(cnf.RedisCfg().Host, cnf.RedisCfg().Port, cnf.RedisCfg().Password)
	cleanupHook()
}

func ConnRds(host string, port int, password string) *redis.Pool {
	hosts := fmt.Sprintf("%s:%d", host, port)

	log.Debug("Redis started:" + hosts)

	return &redis.Pool{
		MaxIdle:   80,
		MaxActive: 12000,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", hosts)
			if err != nil {
				log.Fatal(err)
			}
			if password != "" {
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					return nil, err
				}
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func cleanupHook() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	signal.Notify(c, syscall.SIGKILL)
	go func() {
		<-c
		Pool.Close()
		os.Exit(0)
	}()
}

func FindTokenDevice(token string) (deviceID string, deviceMac string, deviceTopic string) {

	var tokenprof TokenProfile

	c := Pool.Get()
	defer c.Close()

	reply, err := c.Do("GET", "tokenize:"+token)
	if err != nil {
		log.Fatal(err)
		log.Error("Token Not Found")
	}

	log.Notice("GET OK")

	if err = json.Unmarshal(reply.([]byte), &tokenprof); err != nil {
		log.Critical(err)
	}
	return tokenprof.DeviceId, tokenprof.DeviceMac, tokenprof.DeviceTopic
}

func CreateToken(t TokenProfile) {
	t.Timestamp = time.Now()

	c := Pool.Get()
	defer c.Close()

	b, err := json.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}

	reply, err := c.Do("SET", "tokenize:"+t.Token, b)
	if err != nil {
		log.Fatal(err)
	}

	log.Notice("SET ", reply)
}

func FindTokenNode(token string) (devNodeID string, devNodeMac string, devNodeTopic string, devNodeGateway string) {

	var tokennodeprof TokenNodeProfile

	c := Pool.Get()
	defer c.Close()

	reply, err := c.Do("GET", "tokenize:"+token)
	if err != nil {
		log.Fatal(err)
		log.Error("Token Not Found")
	}

	log.Notice("GET OK")

	if err = json.Unmarshal(reply.([]byte), &tokennodeprof); err != nil {
		log.Critical(err)
	}
	return tokennodeprof.DeviceId, tokennodeprof.DeviceMac, tokennodeprof.DeviceTopic, tokennodeprof.DeviceGateway
}

func CreateNodeToken(t TokenNodeProfile) {
	t.Timestamp = time.Now()

	c := Pool.Get()
	defer c.Close()

	b, err := json.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}

	reply, err := c.Do("SET", "tokenize:"+t.Token, b)
	if err != nil {
		log.Fatal(err)
	}

	log.Notice("SET ", reply)
}

func SetExpiry(token string, timeout string) {
	c := Pool.Get()
	defer c.Close()

	expiry, err := c.Do("EXPIRE", "tokenize:"+token, timeout)
	if err != nil {
		log.Fatal(err)
	}

	log.Notice("SET EXPIRE : ", expiry)
	log.Notice("Time Expiry : ", timeout, "Token : ", token)
}
