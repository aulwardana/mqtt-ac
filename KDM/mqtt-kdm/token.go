package main

import (
	"bytes"
	"math/rand"
	"time"
)

type Generator struct {
	N           int
	CacheLength int
	cache       []string
}

func (g *Generator) NewToken() {
	if g.N == 0 {
		g.N = 1000
	}
	if g.CacheLength == 0 {
		g.CacheLength = 200
	}
	g.cache = make([]string, g.N)
	rand.Seed(time.Now().Unix())
	for i := 0; i < g.N; i++ {
		g.cache[i] = RandomString(g.CacheLength)
	}
}

func (g Generator) GetToken(size int) string {
	i := rand.Intn(g.N)
	start := rand.Intn(g.CacheLength - size - 1)
	return g.cache[i][start : start+size]
}

func RandomInt(min, max int) int {
	return rand.Intn(max-min) + min
}

func RandomString(length int) string {
	var buffer bytes.Buffer
	for i := 0; i < length; i++ {
		choice := RandomInt(0, 2)
		switch choice {
		case 0:
			buffer.WriteString(string(RandomInt(48, 57)))
		case 1:
			buffer.WriteString(string(RandomInt(97, 122)))
		case 2:
			buffer.WriteString(string(RandomInt(65, 90)))

		}
	}
	return buffer.String()
}
