package main

import (
	"os"
)

const (
	SecSslHttpPath       = "./.ssl/http/"
	SecSslHttpServerKey  = "./.ssl/http/server.key"
	SecSslHttpServerCert = "./.ssl/http/server.crt"
	SecSslHttpCert       = "./.ssl/http/ca.crt"
)

func InitSSl() {
	if _, err := os.Stat(SecSslHttpPath); err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(SecSslHttpPath, 755)
		}
	}

	if _, err := os.Stat(SecSslHttpServerKey); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().KeyFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Client Key File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().KeyFileDir, SecSslHttpServerKey)
				if err != nil {
					log.Error(err)
				}
			}
		}
	}

	if _, err := os.Stat(SecSslHttpServerCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().CrtFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Client File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().CrtFileDir, SecSslHttpServerCert)
				if err != nil {
					log.Error(err)
				}
			}
		}
	}

	if _, err := os.Stat(SecSslHttpCert); err != nil {
		if os.IsNotExist(err) {
			if _, err := os.Stat(cnf.HttpCfg().CAFileDir); err != nil {
				if os.IsNotExist(err) {
					log.Error("Please Deploy SSl Certificate Authority File for HTTPS Connection")
				}
			} else {
				err := os.Rename(cnf.HttpCfg().CAFileDir, SecSslHttpCert)
				if err != nil {
					log.Error(err)
				}
			}
		}
	}
}

func UpdateHttpSSL() {
	if _, err := os.Stat(cnf.HttpCfg().KeyFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Client Key File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().KeyFileDir, SecSslHttpServerKey)
		if err != nil {
			log.Error(err)
		}
	}

	if _, err := os.Stat(cnf.HttpCfg().CrtFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Client File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().CrtFileDir, SecSslHttpServerCert)
		if err != nil {
			log.Error(err)
		}
	}

	if _, err := os.Stat(cnf.HttpCfg().CAFileDir); err != nil {
		if os.IsNotExist(err) {
			log.Error("Please Deploy SSl Certificate Authority File for HTTPS Connection")
		}
	} else {
		err := os.Rename(cnf.HttpCfg().CAFileDir, SecSslHttpCert)
		if err != nil {
			log.Error(err)
		}
	}
}
