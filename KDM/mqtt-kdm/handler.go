package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

const (
	GET    string = "GET"
	POST   string = "POST"
	PUT    string = "PUT"
	DELETE string = "DELETE"
)

func Route(r *mux.Router, path string, handler func(http.ResponseWriter, *http.Request), method string) {
	r.HandleFunc(path, handler).Methods(method)
}

func Run(r *mux.Router, address string, port int) {
	caCert, err := ioutil.ReadFile(SecSslHttpCert)
	if err != nil {
		log.Fatal(err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	cfg := &tls.Config{
		ClientAuth: tls.RequireAndVerifyClientCert,
		ClientCAs:  caCertPool,
	}
	portHttps := fmt.Sprintf("%s:%d", address, port)
	srv := &http.Server{
		Addr:      portHttps,
		Handler:   r,
		TLSConfig: cfg,
	}

	log.Debug("KDC Server Started: ", srv.Addr)
	log.Error(srv.ListenAndServeTLS(SecSslHttpServerCert, SecSslHttpServerKey))
}

func MainHome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Connected to Authentication Server\n"))
	log.Info("a device has been connected")
}

func AuthAPI(w http.ResponseWriter, r *http.Request) {
	DevId := r.FormValue("dev_id")
	DevPass := r.FormValue("dev_pass")
	DevMac := r.FormValue("dev_mac")
	DevTopic := r.FormValue("dev_topic")
	RealDevPass := CRTRecDec(DevPass)
	if AuthDevice(DevId, RealDevPass, DevMac, DevTopic) == true {
		g := Generator{}
		g.NewToken()
		token := g.GetToken(32)

		CreateToken(TokenProfile{
			Token:       string(token),
			DeviceId:    string(DevId),
			DeviceMac:   string(DevMac),
			DeviceTopic: string(DevTopic),
		})

		SetExpiry(string(token), cnf.KDMCfg().TokenKeepAlive)

		w.Write([]byte(token))
		log.Info("Credential Valid")
		log.Info("Token : ", token)
	} else {
		w.Write([]byte("invalid_auth"))
	}
}

func ValidateAPI(w http.ResponseWriter, r *http.Request) {
	PubToken := r.FormValue("pub_token")
	if ValidateToken(PubToken) == true {
		w.Write([]byte("valid"))
		log.Info("Token Verified")
	} else {
		w.Write([]byte("invalid"))
		log.Info("Token Invalid")
	}
}

func RegAPI(w http.ResponseWriter, r *http.Request) {
	DevId := r.FormValue("dev_id")
	DevMac := r.FormValue("dev_mac")
	DevTopic := r.FormValue("dev_topic")

	if CheckCredential(DevId, DevMac, DevTopic) == true {
		w.Write([]byte("is_registered"))
	} else {
		g := Generator{}
		g.NewToken()
		DevPass := g.GetToken(8)
		chippass := CRTSendEnc(DevPass)

		GetCredential(DevPass, DevMac)
		w.Write([]byte(chippass))
	}
}

func CheckAPI(w http.ResponseWriter, r *http.Request) {
	DevId := r.FormValue("dev_id")
	DevMac := r.FormValue("dev_mac")
	DevTopic := r.FormValue("dev_topic")

	if CheckDevice(DevId, DevMac, DevTopic) == true {
		w.Write([]byte("registered"))
	} else {
		w.Write([]byte("not_registered"))
	}
}

func CheckNodeAuthAPI(w http.ResponseWriter, r *http.Request) {
	DevId := r.FormValue("dev_id")
	DevPass := r.FormValue("dev_pass")
	DevMac := r.FormValue("dev_mac")
	DevTopic := r.FormValue("dev_topic")
	RealDevPass := CRTRecDec(DevPass)

	NodeId := r.FormValue("node_id")
	NodePass := r.FormValue("node_pass")
	NodeMac := r.FormValue("node_mac")
	NodeTopic := r.FormValue("node_topic")

	if AuthDevice(DevId, RealDevPass, DevMac, DevTopic) == true {
		if CheckNode(NodeId, NodePass, NodeMac, NodeTopic, DevId) == true {
			g := Generator{}
			g.NewToken()
			token := g.GetToken(16)

			CreateNodeToken(TokenNodeProfile{
				Token:         string(token),
				DeviceId:      string(NodeId),
				DeviceMac:     string(NodeMac),
				DeviceTopic:   string(NodeTopic),
				DeviceGateway: string(DevId),
			})

			SetExpiry(string(token), cnf.KDMCfg().TokenKeepAlive)

			w.Write([]byte(token))
			log.Info("Credential Valid")
			log.Info("Token : ", token)
		} else {
			w.Write([]byte("not_registered"))
		}
	} else {
		w.Write([]byte("gateway_not_registered"))
	}
}

func CheckNodeTokenAPI(w http.ResponseWriter, r *http.Request) {
	DevId := r.FormValue("dev_id")
	DevPass := r.FormValue("dev_pass")
	DevMac := r.FormValue("dev_mac")
	DevTopic := r.FormValue("dev_topic")
	RealDevPass := CRTRecDec(DevPass)

	NodePubToken := r.FormValue("node_pub_token")

	if AuthDevice(DevId, RealDevPass, DevMac, DevTopic) == true {
		if ValidateNodeToken(NodePubToken) == true {
			w.Write([]byte("valid"))
			log.Info("Token Verified")
		} else {
			w.Write([]byte("invalid"))
			log.Info("Token Invalid")
		}
	} else {
		w.Write([]byte("gateway_not_registered"))
	}
}
