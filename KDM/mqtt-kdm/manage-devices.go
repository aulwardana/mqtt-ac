package main

import (
	"database/sql"
	"fmt"
	"sync"
	"time"

	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

type DeviceProfile struct {
	Uid      int
	DevId    string
	DevMac   string
	DevPass  string
	DevTopic string
	RegTime  time.Time
}

type NodeProfile struct {
	Uid         int
	NodeId      string
	NodePass    string
	NodeMac     string
	NodeTopic   string
	NodeGateway string
	RegTime     time.Time
}

var db *sql.DB
var once sync.Once

func GetConnection() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s", cnf.PostgresCfg().Host, cnf.PostgresCfg().Port, cnf.PostgresCfg().User, cnf.PostgresCfg().Password, cnf.PostgresCfg().DBName, cnf.PostgresCfg().SSLMode)

	once.Do(func() {
		var err error
		if db, err = sql.Open("postgres", psqlInfo); err != nil {
			log.Panic(err)
		}
		db.SetMaxOpenConns(20)
		db.SetMaxIdleConns(0)
		db.SetConnMaxLifetime(time.Nanosecond)
	})
	return db
}

func CheckDevice(devId string, devMac string, devTopic string) (status bool) {

	tx, err := db.Begin()
	defer tx.Rollback()
	rows, err := db.Query("SELECT * FROM public.devices WHERE devmac = $1", devMac)
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer rows.Close()

	for rows.Next() {
		c := DeviceProfile{}
		err := rows.Scan(&c.Uid, &c.DevId, &c.DevPass, &c.DevMac, &c.DevTopic, &c.RegTime)
		if err != nil {
			log.Fatal(err)
		}

		if c.DevMac == devMac && c.DevId == devId && c.DevTopic == devTopic {
			return true
		} else {
			return false
		}
	}

	return false
}

func CheckCredential(devId string, devMac string, devTopic string) (status bool) {

	tx, err := db.Begin()
	defer tx.Rollback()
	rows, err := db.Query("SELECT * FROM public.devices WHERE devmac = $1", devMac)
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer rows.Close()

	for rows.Next() {
		c := DeviceProfile{}
		err := rows.Scan(&c.Uid, &c.DevId, &c.DevPass, &c.DevMac, &c.DevTopic, &c.RegTime)
		if err != nil {
			log.Fatal(err)
		}

		if c.DevPass == "" {
			return false
		} else {
			return true
		}
	}

	return false
}

func AuthDevice(devId string, devPass string, devMac string, devTopic string) (status bool) {

	tx, err := db.Begin()
	defer tx.Rollback()
	rows, err := db.Query("SELECT * FROM public.devices WHERE devid = $1", devId)
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer rows.Close()

	for rows.Next() {
		c := DeviceProfile{}
		err := rows.Scan(&c.Uid, &c.DevId, &c.DevPass, &c.DevMac, &c.DevTopic, &c.RegTime)
		if err != nil {
			log.Fatal(err)
		}

		if c.DevMac == devMac && c.DevId == devId && c.DevTopic == devTopic {
			if comparePasswords(c.DevPass, []byte(devPass)) == true {
				return true
			} else {
				return false
			}
		} else {
			return false
		}
	}

	return false
}

func GetCredential(devPass string, devMac string) {
	regTime := time.Now()
	hashpaswd := hashAndSalt([]byte(devPass))

	_, err := db.Exec("UPDATE public.devices set devpass = $1, regtime = $2 where devmac = $3", hashpaswd, regTime, devMac)
	if err != nil {
		log.Error(err)
		log.Info("Get Credential Failed")
	} else {
		log.Info("Get Credential Success", devMac)
	}
}

func CheckNode(nodeId string, nodePass string, nodeMac string, nodeTopic string, nodeGateway string) (status bool) {

	tx, err := db.Begin()
	defer tx.Rollback()
	rows, err := db.Query("SELECT * FROM public.nodes WHERE devmac = $1", nodeMac)
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer rows.Close()

	for rows.Next() {
		c := NodeProfile{}
		err := rows.Scan(&c.Uid, &c.NodeId, &c.NodePass, &c.NodeMac, &c.NodeTopic, &c.NodeGateway, &c.RegTime)
		if err != nil {
			log.Fatal(err)
		}

		if c.NodeMac == nodeMac && c.NodeId == nodeId && c.NodeTopic == nodeTopic && c.NodeGateway == nodeGateway {
			if comparePasswords(c.NodePass, []byte(nodePass)) == true {
				return true
			} else {
				return false
			}
		} else {
			return false
		}
	}

	return false
}

func CheckNodeDev(nodeId string, nodeMac string, nodeTopic string, nodeGateway string) (status bool) {

	tx, err := db.Begin()
	defer tx.Rollback()
	rows, err := db.Query("SELECT * FROM public.nodes WHERE devmac = $1", nodeMac)
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer rows.Close()

	for rows.Next() {
		c := NodeProfile{}
		err := rows.Scan(&c.Uid, &c.NodeId, &c.NodePass, &c.NodeMac, &c.NodeTopic, &c.NodeGateway, &c.RegTime)
		if err != nil {
			log.Fatal(err)
		}

		if c.NodeMac == nodeMac && c.NodeId == nodeId && c.NodeTopic == nodeTopic && c.NodeGateway == nodeGateway {
			return true
		} else {
			return false
		}
	}

	return false
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Error(err)
	}

	return string(hash)
}

func comparePasswords(hashedPwd string, plainPwd []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Error(err)
		return false
	}

	return true
}
