# KDM (Key Distribution Management) App


This application use to manage credential (token) for device connection. This application also handle registration device, authentication device, and verify credential.


## VPS Preparation
1. Use VM or VPS with [Cent OS](https://www.centos.org/download/ "Cent OS") installed inside.
2. Install PostgreSQL. You can use pgAdmin to remote the PostgreSQL for development purpose. Download via [this link](https://www.pgadmin.org/ "pgAdmin").
3. Install Redis. You can use Redis Desktop Manager to remote the Redis for development purpose. Download via [this link](https://redisdesktop.com/ "RDM").
4. Install OpenSSL.
5. Open port 443 in VM or VPS.


  ``` bash
  sudo firewall-cmd --permanent --add-port=443/tcp
  sudo firewall-cmd --reload
  ```


## Generate SSL Certificate
1. Log in to authentication server VPS via SSH.
2. Generate SSL Certificate using our script in `mqtt-ac/Tools/` folder.
3. For development in localhost you can use SSL Certificate in `master/Tools/ssl-cert` folder.


## Config File
Please configure the config file `config-sample.yaml` in `mqtt-ac/KDM/mqtt-kdm` folder, sample configuration is like this :


  ``` bash
  kdm: 
    token-keepalive: "5"
  http: 
    protocol: "https"
    address: "104.154.195.155"
    port: 443
    cafiledir: "ssl-cert/http/client2.crt"
    crtfiledir: "ssl-cert/http/server2.crt"
    keyfiledir: "ssl-cert/http/server2.key"
  pg: 
    host: "localhost"
    port: 5432
    user: "postgres"
    password: "postgres"
    dbname: "postgres"
    sslmode: "disable"
  rds: 
    host: "localhost"
    port: 6379
    password: "rahasia"
  ```


After configuration finish, please rename this configuration with `config.yaml` and add this configuration in same folder with KDM app.


## Deploy and Allow Port 443
First, build application to binary, then deploy the KDM app to VPS or VM with secure copy (SCP) command. Then, use this command to allow application to use port 443.


  ``` bash
  sudo setcap CAP_NET_BIND_SERVICE=+eip ~/mqtt-kdm
  ```


When KDM App running, it will be listen the conection between publisher and subscriber that connect to authentication server. This app must be set in first step configuration before middleware setup.
