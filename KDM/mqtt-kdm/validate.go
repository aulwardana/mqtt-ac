package main

func ValidateToken(token string) (status bool) {

	DeviceIdToken, DeviceMacToken, DeviceTopicToken := FindTokenDevice(token)

	log.Info(DeviceIdToken, DeviceMacToken, DeviceTopicToken)

	if CheckDevice(DeviceIdToken, DeviceMacToken, DeviceTopicToken) == true {
		return true
	} else {
		return false
	}

	return false
}

func ValidateNodeToken(token string) (status bool) {

	DevNodeIdToken, DevNodeMacToken, DevNodeTopicToken, DevNodeMacGateway := FindTokenNode(token)

	log.Info(DevNodeIdToken, DevNodeMacToken, DevNodeTopicToken, DevNodeMacGateway)

	if CheckNodeDev(DevNodeIdToken, DevNodeMacToken, DevNodeTopicToken, DevNodeMacGateway) == true {
		return true
	} else {
		return false
	}

	return false
}
