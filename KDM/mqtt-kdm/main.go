package main

import (
	"flag"
	"os"

	"github.com/gorilla/mux"
	"github.com/op/go-logging"
)

var (
	cnf        *Config
	configPath string
)

var log = logging.MustGetLogger("secure-log-style")

var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:05x}%{color:reset} %{message}`,
)

/*
Options:
 [-u ssl]               	Action ssl (update http ssl)
 [-c <path/config.yaml>]    Use custom configuration file (config.yaml)
*/

func initConfig() error {
	flag.StringVar(&configPath, "c", "config.yaml", "Configuration File")

	c, err := NewCfg(configPath)
	if err != nil {
		return err
	}
	cnf = c

	return err
}

func main() {
	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend2 := logging.NewLogBackend(os.Stderr, "", 0)

	backend2Formatter := logging.NewBackendFormatter(backend2, format)
	backend1Leveled := logging.AddModuleLevel(backend1)
	backend1Leveled.SetLevel(logging.ERROR, "")

	logging.SetBackend(backend1Leveled, backend2Formatter)

	err := initConfig()
	if err != nil {
		log.Fatal(err)
	}

	update := flag.String("u", "", "Action http (update http ssl) or mqtt (update mqtt ssl)")
	flag.Parse()

	if *update == "ssl" {
		UpdateHttpSSL()
	} else {
		log.Info("Invalid setting for -update ssl, must be ssl")
	}

	InitSSl()

	db = GetConnection()

	RdsInit()

	CRTCheck()

	r := mux.NewRouter()

	Route(r, "/", MainHome, GET)
	Route(r, "/auth/", AuthAPI, POST)
	Route(r, "/validate/", ValidateAPI, POST)
	Route(r, "/reg/", RegAPI, POST)
	Route(r, "/check/", CheckAPI, POST)
	Route(r, "/node-auth/", CheckNodeAuthAPI, POST)
	Route(r, "/node-token/", CheckNodeTokenAPI, POST)

	Run(r, cnf.HttpCfg().Address, cnf.HttpCfg().Port)
}
