package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
	"io/ioutil"
	"os"
)

const (
	crtFolderPath = "./.crt/"
	AESKeyPath    = "./.crt/aes.aul"
)

var (
	AESKey string
)

func CRTCheck() {
	if _, err := os.Stat(crtFolderPath); err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll("./.crt/", 755)
		}
	}

	if _, err := os.Stat(AESKeyPath); err != nil {
		if os.IsNotExist(err) {
			os.Create(AESKeyPath)
			g := Generator{}
			g.NewToken()
			ioutil.WriteFile(AESKeyPath, []byte(g.GetToken(16)), 755)
		}
	}

	AESKeyGen()
}

func CRTSendEnc(credentialStr string) string {
	ciphertext := EncryptSTR(credentialStr, AESKey)

	return ciphertext
}

func CRTRecDec(ciphertext string) string {
	credentialStr := DecryptSTR(ciphertext, AESKey)

	return credentialStr
}

func AESKeyGen() {
	data, err := ioutil.ReadFile(AESKeyPath)
	if err != nil {
		panic(err)
	}
	AESKey = string(data)
}

func EncryptSTR(plainstring, keystring string) string {
	plaintext := []byte(plainstring)
	AESKey := []byte(keystring)

	block, err := aes.NewCipher(AESKey)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	return string(ciphertext)
}

func DecryptSTR(cipherstring string, keystring string) string {
	ciphertext := []byte(cipherstring)
	AESKey := []byte(keystring)

	block, err := aes.NewCipher(AESKey)
	if err != nil {
		panic(err)
	}

	if len(ciphertext) < aes.BlockSize {
		panic("Text is too short")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)

	return string(ciphertext)
}
