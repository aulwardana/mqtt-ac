package main

import (
	"bytes"
	"io/ioutil"
	"os"

	"github.com/spf13/viper"
)

const (
	KDM_TOKEN_KEEPALIVE string = "kdm.token-keepalive"
	HTTP_PROTOCOL       string = "http.protocol"
	HTTP_ADDRESS        string = "http.address"
	HTTP_PORT           string = "http.port"
	HTTP_CAFILEDIR      string = "http.cafiledir"
	HTTP_CRTFILEDIR     string = "http.crtfiledir"
	HTTP_KEYFILEDIR     string = "http.keyfiledir"
	PG_HOST             string = "pg.host"
	PG_PORT             string = "pg.port"
	PG_USER             string = "pg.user"
	PG_PASSWORD         string = "pg.password"
	PG_DBNAME           string = "pg.dbname"
	PG_SSLMODE          string = "pg.sslmode"
	RDS_HOST            string = "rds.host"
	RDS_PORT            string = "rds.port"
	RDS_PASSWORD        string = "rds.password"
)

type KDMConfig struct {
	TokenKeepAlive string
}

type HttpConfig struct {
	Protocol   string
	Address    string
	Port       int
	CAFileDir  string
	CrtFileDir string
	KeyFileDir string
}

type PgConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	DBName   string
	SSLMode  string
}

type RdsConfig struct {
	Host     string
	Port     int
	Password string
}

type Config struct {
	*viper.Viper
}

func setDefaults(v *viper.Viper) {
	v.SetDefault(KDM_TOKEN_KEEPALIVE, "5")
	v.SetDefault(HTTP_PROTOCOL, "https")
	v.SetDefault(HTTP_ADDRESS, "localhost")
	v.SetDefault(HTTP_PORT, 8443)
	v.SetDefault(HTTP_CAFILEDIR, "ssl-cert/http/CAfile.pem")
	v.SetDefault(HTTP_CRTFILEDIR, "ssl-cert/http/client-crt.pem")
	v.SetDefault(HTTP_KEYFILEDIR, "ssl-cert/http/client-key.pem")
	v.SetDefault(PG_HOST, "localhost")
	v.SetDefault(PG_PORT, 5432)
	v.SetDefault(PG_USER, "postgres")
	v.SetDefault(PG_PASSWORD, "postgres")
	v.SetDefault(PG_DBNAME, "postgres")
	v.SetDefault(PG_SSLMODE, "disable")
	v.SetDefault(RDS_HOST, "localhost")
	v.SetDefault(RDS_PORT, 6379)
	v.SetDefault(RDS_PASSWORD, "admin")
}

func loadConfPath(v *viper.Viper, path string) error {
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	f, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	return v.ReadConfig(bytes.NewBuffer(f))
}

func (c *Config) KDMCfg() *KDMConfig {
	return &KDMConfig{
		TokenKeepAlive: c.GetString(KDM_TOKEN_KEEPALIVE),
	}
}

func (c *Config) HttpCfg() *HttpConfig {
	return &HttpConfig{
		Protocol:   c.GetString(HTTP_PROTOCOL),
		Address:    c.GetString(HTTP_ADDRESS),
		Port:       c.GetInt(HTTP_PORT),
		CAFileDir:  c.GetString(HTTP_CAFILEDIR),
		CrtFileDir: c.GetString(HTTP_CRTFILEDIR),
		KeyFileDir: c.GetString(HTTP_KEYFILEDIR),
	}
}

func (c *Config) PostgresCfg() *PgConfig {
	return &PgConfig{
		Host:     c.GetString(PG_HOST),
		Port:     c.GetInt(PG_PORT),
		User:     c.GetString(PG_USER),
		Password: c.GetString(PG_PASSWORD),
		DBName:   c.GetString(PG_DBNAME),
		SSLMode:  c.GetString(PG_SSLMODE),
	}
}

func (c *Config) RedisCfg() *RdsConfig {
	return &RdsConfig{
		Host:     c.GetString(RDS_HOST),
		Port:     c.GetInt(RDS_PORT),
		Password: c.GetString(RDS_PASSWORD),
	}
}

func NewCfg(path ...string) (*Config, error) {
	v := viper.New()
	v.SetConfigType("yaml")
	setDefaults(v)

	if len(path) > 0 {
		if err := loadConfPath(v, path[0]); err != nil {
			log.Warning("Failed load from file. Authentication Server using default configuration")
		}
	}

	return &Config{v}, nil
}
