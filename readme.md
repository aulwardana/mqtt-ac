# MQTT-AC


Access Control middleware for device (node and gateway) to cloud connection in Internet of Things Technology based on MQTT Protocol using Authentication Server and Secure Protocol. This middleware ideally implement in fog computing architecture.


## Description


MQTT-AC is security tool that use to give access control to device (node and gateway) that connect to cloud to validate the sensing data. This middleware securing the publish/subscribe communication form MQTT protocol using credential that give to device from authentication server and securing communication between device (publisher) to cloud (subscriber) using SSL/TLS that generate and distribute from authentication server. For more details about architecture and module connection please see the [wiki](https://gitlab.com/aulwardana/mqtt-ac/wikis/home "MQTT-AC Wiki") from this repository.


## Development Tool


For development purpose, I recomended to use [Fedora Workstation](https://getfedora.org/id/workstation/ "Fedora Workstation") for develop this application because the source is RHEL and have same source with [Cent OS](https://www.centos.org/download/ "Cent OS") as VPS operating system. After that, please install this tool first for develop this app :
1. Golang Intepreter, download via [this link](https://golang.org/dl/ "Go Programing Language").
2. Mosquitto MQTT, download via [this link](http://mosquitto.org/ "Eclipse Mosquitto™").
3. OpenSSL, download via [this link](https://www.openssl.org/ "OpenSSL").
4. PostgreSQL, download via [this link](https://www.postgresql.org/ "PostgreSQL").
5. Redis, download via [this link](https://redis.io/ "Redis Key Value Store").
6. Espressif, download via [this link](https://github.com/espressif/arduino-esp32 "Arduino ESP32 Chip Library"). 

For code management, I recomended this tool :
1. Git, download via [this link](https://git-scm.com/ "Git Version Control").
2. LiteIDE (golang compiler and editor), download via [this link](https://github.com/visualfc/liteide "LiteIDE").
3. Visual Studio Code, download via [this link](https://code.visualstudio.com/ "Visual Studio Code").
4. Arduino IDE, download via [this link](https://www.arduino.cc/en/Main/Software "Arduino IDE").


## External Library
1. Router URL for HTTP: gorilla/mux 


    ``` bash
    $ go get github.com/gorilla/mux
    ```


2. Logging System: go-logging 


    ``` bash
    $ go get github.com/op/go-logging
    ```


3. PostgreSql Database Driver: pq


    ``` bash
    $ go get github.com/op/go-logging
    ```


4. Redis Database Driver: redigo


    ``` bash
    $ go get github.com/garyburd/redigo/redis
    ```



5. System Config: viper


    ``` bash
    $ go get github.com/spf13/viper
    ```


6. MQTT Driver: Paho MQTT


    ``` bash
    $ go get github.com/eclipse/paho.mqtt.golang
    ```


## Prepare for Development


After you install all development tool and get all external library, you can clone this project with :

``` bash
$ cd ~/gopath/src/
$ git clone https://gitlab.com/aulwardana/mqtt-ac.git
```

open `mqtt-ac` folder with LiteIDE or Visual Studio Code for code editor.


## Recommended Device
1. Device Gateway : Raspberry Pi 3 model B, 2 model B, and B/B+. You can see the hardware specification [here](https://www.raspberrypi.org/products/ "Raspberry Pi"). For Raspberry Pi 2 model B and B/B+, you must add WiFi dongle for create node network via WiFi. I recommended [this](https://www.amazon.com/gp/product/B003MTTJOY/ "Edimax") WiFi dongle device for Raspberry. 
2. Device Gateway OS : Raspbian OS with active SSH. You can download the operating system [here](https://www.raspberrypi.org/downloads/raspbian/ "Raspbian OS").
3. Device Node : ESP32-DevKitC. You can see the hardware specification [here](https://www.espressif.com/en/products/hardware/esp32-devkitc/overview "ESP32-DevKitC").


## Download MQTT-AC


You can download latest mqtt-ac with look in this [realease tag](https://gitlab.com/aulwardana/mqtt-ac/tags "MQTT-AC Release"). This build tested in linux based redhat and raspbian operating system.


## Deployment Configuration
1. Follow [this link](https://gitlab.com/aulwardana/mqtt-ac/tree/master/KDM/mqtt-kdm "KDM App") for KDM (key distribution management) application in authentication server setup.
1. Follow [this link](https://gitlab.com/aulwardana/mqtt-ac/tree/master/KDM/mqtt-reg-dev "Registration App") for Registration application for device node and device gateway in authentication server setup.
2. Follow [this link](https://gitlab.com/aulwardana/mqtt-ac/tree/master/Middleware/mqtt-ac "Middleware App") for Publisher (device gateway) or Subscriber (cloud server) Middleware application setup.
3. Follow [this link](https://gitlab.com/aulwardana/mqtt-ac/tree/master/Middleware/mqtt-node "NKM App") for NKM (node key manager) application in device gateway setup.
3. Follow [this link](https://gitlab.com/aulwardana/mqtt-ac/tree/master/Middleware/arduino "Secure Publish Library") for Secure Publish library in device node setup.


## IEEE Paper


You can get our paper in IEEE, link is [available here](https://ieeexplore.ieee.org/document/8534855 "IEEE Conference Paper").


## Acknowledgement


Gratitude to the **Ministry of Education and Culture of The Republic of Indonesia** who has granted **"Beasiswa Unggulan"** scholarship to the first author so he can continue his studies to the master program at Institut Teknologi Bandung. Hopefully, this research can contribute to the advancement of technology research in Indonesia.

